# Class Assignment 4

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte II - *Containers* com *docker compose*

<br>

### O que é e para que serve o *docker compose*?

O **Docker compose** é uma funcionalidade Docker que permite criar, executar e gerir vários *containers* Docker. Através de um ficheiro *docker-compose* com extensão YAML é possível configurar vários ***services*** (um *service* corresponde a um *container*). De uma só vez, é possível criar e executar os vários serviços configurados.

O Compose funciona em vários ambientes: *production*, *staging*, *development*, *testing* e *workflows CI*. É habitualmente utilizado em contexto de desenvolvimento, pois permite experimentar diferentes ambientes para a aplicação, isto é, experimentar a aplicação com dependências e frameworks diferentes, como por exemplo bases de dados, web services API's, etc. É também muito útil para cenários de *Automated Tests*.

Antes de se avançar para o relatório técnico, apresentam-se alguns comandos habitualmente utilizados **Docker compose**:

<br>

| Comando  | Descrição                                                  |
|:-----------:|:-------------:
| *docker-compose build* | Constrói as *images* |   
| *docker-compose up* | Cria e inicia os *containers* |
| *docker-compose start/stop [service name]* | Incia/Pára os *containers*|
| *docker-compose ps* | Lista os *containers* |
| *docker-compose exec [service name] [command]* | Executa o comando definido no service escolhido |
| *docker-compose rm [Service name]* | Remove o *container* definido |
| *docker-compose images* | Lista as *images* |

<br>


## Relatório técnico - Criar múltiplos *containers* com *docker compose*

O objectivo desta **Parte II** é executar a aplicação Spring desenvolvida no CA2 - Parte II num ambiente "containerizado" através da funcionalidade *docker compose*. Serão criados dois containers:

- **web** - Que vai executar a aplicação Spring no servidor *Tomcat*;
- **db** - Que vai executar num servidor de base de dados *H2*

### 1. Criar containers *db* e *web*
**1.1.** Começamos por criar os directórios para cada container - *web* e *db*.

**1.1.1.** O *container* **web** será criado a partir do seguinte *Dockerfile*:

```
FROM tomcat:9.0-jdk11-temurin

RUN apt-get update -y 


RUN apt-get install git nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /temp

WORKDIR /temp

RUN git clone https://hugomorgadinho@bitbucket.org/hugomorgadinho/devops-21-22-lmn-1211766.git

WORKDIR devops-21-22-lmn-1211766/CA2/Part_2/react-and-spring-data-rest-basic

RUN chmod u+x gradlew

RUN ./gradlew clean build && cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ && rm -rf /temp

EXPOSE 8080

```

Uma vez que estamos a utilizar o jdk 11 na aplicação Spring, a imagem base para o tomcat é a `tomcat:9.0-jdk11-temurin`. 

**1.1.2** O *container* **db** será criado a partir do seguinte *Dockerfile*:

```
FROM ubuntu:18.04

RUN apt-get update -y \
    && apt-get install -y openjdk-11-jdk-headless \
    && apt-get install unzip -y \
    && apt-get install wget -y


RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

```
A imagem base para o container *db* será a versão 18.04 do ubuntu, à semelhança do que utilizámos com Vagrant.

<br>

**1.2.** De seguida criamos o ficheiro *yaml* para o *docker-compose* que irá gerar os containers definidos nos dockerfile anteriores:

```
version: "3"
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.56.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.56.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.56.0/24

```

**1.3.** É necessário confirmar que o endereço ip definido no *Dockerfile* *db*, corresponde ao endereço definido no ficheiro *application.properties* da aplicação *Spring*.

**1.4.** Podemos agora construir as *images*. Para isso, na pasta onde criámos o ficheiro yaml, executamos o comando `docker-compose build`.

**1.5.** E a seguir, executar os *containers*: `docker-compose up`.

**1.6.** Quando os *containers* estiverem em modo *run*, é possível confirmar na máquina *host* que a aplicação está em andamento.

**1.6.1.** Para visualizar o frontend da aplicação, abrimos o url: `http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT`;

**1.6.2.** A base de dados pode ser acedida através do url: `http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console`.

- Para a entrar na *db*, utilizamos `jdbc:h2:tcp://192.168.56.11:9092/./jpadb`. 


- É então possível adicionar novos dados e visualizar o resultado no frontend.

<br>

### 2. Utilizar um *volume* para o *container* db

**2.1** Com os containers em modo *run*, abrimos um novo terminal e executamos o comando: ` docker-compose exec db sh`.

**2.2** A nova *prompt* indica que estamos na *shell* do container *db*. Para copiar o backup da base de dados, executa-se o comando `cp jpadb.mv.db /usr/src/data-backup`. O backup será copiado para uma pasta partilhada com o *host*

**2.3** Feita a cópia, para sair da *shell* do container basta executar `exit`.

### 3. Tag *images* para publicar no Docker Hub

Para publicar as imagens criadas no Docker Hub:

**3.1.** Fazemos o login executando `docker login`. Se necessário, introduzir o respectivo *username* e *password*.

**3.2.** Assinalar a tag na imagem web: `docker tag part_2_web hugoamp/ca4-part2_web:version1.0`

**3.3.** Fazer o push da *image* web: `docker push hugoamp/ca4-part2_web:version1.0`

**3.4.** Assinalar a tag na imagem db: `docker tag part_2_db hugoamp/ca4-part2_db:version2.0`

**3.5.** Fazer o push da *image* db: `docker push hugoamp/ca4-part2_db:version1.0`

