# Class Assignment 4

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte II alternativa - "Orquestração" de *containers* com Kubernetes

<br>

### 1. O que é Kubernetes?

**Kubernetes** é uma ferramenta *open-source* que permite gerir de forma automatizada *containers* Linux. Desenvolvida inicialmente pela Google, esta plataforma possibilita fazer o *deployment* de aplicações sem necessidade de se executar um conjunto de processos manuais habitualmente necessários.

É possível agrupar em *clusters* os *hosts* executados nos *containers*. Estes *hosts* podem estar em diferentes ambientes, isto é, Kubernetes permite gerir *containers* em máquinas físicas ou virtuais, ou mesmo em ambiente *cloud*. Por esta razão, Kubernetes é considerada a plataforma ideal para hospedar aplicações em nuvem que exigem uma escalabilidade eficaz.

Kubernetes veio ajudar no desenvolvimento assente em *microservices*. Com a crescente utilização de *containers* no processo de desenvolvimento e o aumento da respectiva complexidade desses ambientes, a utilização de uma ferramenta de orquestração de *containers* tornou-se muito necessária.

<br>

### 2. Arquitectura: Como funciona?

Existem alguns conceitos importantes em Kubernetes e que definem a sua arquitectura. Apresentam-se os principais:
 
- ***Node***:Um *node* (nó) é uma máquina (física ou virtual). Tem a responsabilidade de executar um ou vários *containers* onde é feito o *deployment* de determinada aplicação. O conjunto de nós do *cluster* é designado por *worker nodes* ou *workers*;


- ***Master***: É o nó que controla todos os nós. É nele que se definem todas as atribuições de tarefas a realizar. Tem a responsabilidade de controlar a alocação de recursos no cluster. O conjunto de nós Master forma o que pode ser considerado o cérebro de um *cluster* Kubernetes: o Control Plane;


- ***etcd***: É outro tipo de nó, responsável pelo armazenamento de toda a actividade dentro do *cluster*. É no fundo a base de dados distribuída do *cluster*;


- ***Control Plane***: É responsável pela gestão do sistema, de todos os ambientes. É fundamental para garantir que tudo funciona de acordo com o estado desejado para a aplicação;


- ***Pod***: É a unidade básica de um *cluster* Kubernetes. É uma abstracção que representa um ou vários *containers* de uma aplicação. Representa um processo dentro do *cluster*. Pode ser criado ou destruído, dependendo das necessidades da aplicação. Um ou vários *pods* constituem um *node*.


- ***Service***: "Encapsula" um ou mais *pods* e é capaz de encontrá-los dinamicamente em qualquer nó do *cluster*;


- ***Kubelet***: Um processo Kubernetes que torna possível a comunicação no *cluster* e a execução de tarefas nos nós.
 

- ***Virtual network***: Todos os nós estão ligados por uma rede virtual. Permite então que todos os nós sejam um "organismo", uma máquina única;

<br>


A imagem seguinte ilustra um *cluster* e grande parte dos conceitos referidos anteriormente:

![ca3_web1](images/kubernetes-cluster.png)
*Imagem retirada de: https://www.opsramp.com/why-kubernetes/kubernetes-architecture/*

<br>


### 3. Possível implementação de Kubernetes no CA4


**3.1.** Para criar um *cluster* Kubernetes local é necessário instalar a ferramenta Minikube, que no fundo é uma implementação Kubernetes que cria VM de forma local e aí faz o *deployment* de um *cluster* muito básico. É necessário ter o Docker instalado.


**3.2.** Para instalar o Minikube, executa-se o comando: 
````
New-Item -Path 'c:\' -Name 'minikube' -ItemType Directory -Force
Invoke-WebRequest -OutFile 'c:\minikube\minikube.exe' -Uri 'https://github.com/kubernetes/minikube/releases/latest/download/minikube-windows-amd64.exe' -UseBasicParsing
````

**3.3** De seguida, adicionar `minikube.exe` ao nosso `PATH`:

```
$oldPath = [Environment]::GetEnvironmentVariable('Path', [EnvironmentVariableTarget]::Machine)
if ($oldPath.Split(';') -inotcontains 'C:\minikube'){ `
  [Environment]::SetEnvironmentVariable('Path', $('{0};C:\minikube' -f $oldPath), [EnvironmentVariableTarget]::Machine) `
}

```

**3.4.** A seguir, para então iniciar o cluster: `minikube start`


**3.5.** Para aceder ao *cluster* é necessário ter instalado a ferramenta de linha de comando **kubectl**. Executa-se então o comando: `minikube kubectl -- get po -A`


**3.6.** Através do comando `minikube status` é possível conhecer as configurações e o estado do nosso *cluster*.


**3.7** À semelhança do que fizemos com *docker compose*, é necessário criar configurar o *cluster* através de num ficheiro YAML. Neste caso, ainda que com poucos conhecimentos, vamos tentar arriscar e definir um ficheiro para *deployment* de **db** e outro para **web**. Desta maneira isolamos frontend e backend:

Ficheiro YAML para **db**:

````
apiVersion: v1
kind: Deployment
metadata:
  name: springboot-react-app
spec:
  replicas: 1
  volumes:
    - name: data-backup
      emptyDir: {}
  containers:
  - name: db
    image: part_2_db
      volumeMounts:
        - name: data-backup
          mountPath: /usr/src/data-backup
    ports:
      - containerPort: "8082"

    ---

apiVersion: v1
kind: Service
metadata:
  name: springboot-react-app
spec:
  ports:
    - port: 8082
    nodePort: 8091
      protocol: TCP
      targetPort: 8082
  type: NodePort
````

Ficheiro YAML ara **web**:

```
apiVersion: v1
kind: Deployment
metadata:
  name: springboot-react-app
spec:
  replicas: 1
  containers:
  - name: web
      image: part_2_web
      ports:
    - containerPort: "8080"

    ---

apiVersion: v1
kind: Service
metadata:
  name: springboot-react-app
spec:
  ports:
    - port: 8080
    nodePort: 8090
      protocol: TCP
      targetPort: 8080
  type: NodePort
```

**3.8** - Se tudo estiver correcto (o que se espera), pode fazer-se o *deployment*:

```
kubectl apply -f db.yaml
kubectl apply -f web.yaml
```

**3.9** - Para verificar que os *pods* estão em modo running: `kubectl get pods`.


**4.4** - Por fim, podemos verificar no browser a nossa aplicação: `http://localhost:8090/`.


### Conclusão

De um modo geral, podemos dizer que **Docker** é uma plataforma de criação, partilha e execução de *containers*. Já **Kubernetes** é uma ferramenta mais complexa, que pode ser utilizada como um sistema de "orquestração" de ambientes "containerizados", criados em plataformas como Docker.

Ainda que com Docker seja possível criar uma estrutura standard containerizada para um microsserviço, Docker não resolve alguns possíveis problemas, como por exemplo, fazer um upgrade a uma aplicação sem interromper o serviço ou acompanhar o estado da aplicação e substituir um serviço caso algo tenha corrido mal.

Criar *containers* é o domínio do Docker. De maneira simples e rápida, como experimentámos neste CA4, criamos *containers* para fazer o *deployment* da nossa aplicação. Mas o *software* hoje em dia consiste em dezenas ou centenas de *containers* e para operar esta rede complexa é necessária uma ferramenta como Kubernetes, que oferece um conjunto de mecanismos de coordenação desses *containers*.

Docker e Kubernetes são duas tecnologias que se complementam. Têm diferentes finalidades. A partir de uma podemos criar ambientes específicos para microsserviços, e com a outra coordenar todas as operações e infraestruturas.