# Class Assignment 4

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte I - Introdução a Containers e Docker

<br>

### O que são *containers*?

*Containers* são ambientes virtuais isolados habitualmente utilizados para “empacotar” aplicações. A grande diferença relativamente às máquinas virtuais - *virtual machines (VM)* - é focar-se mais no conceito de aplicação e menos no conceito de máquina (computador). Isto é, funciona ao nível do sistema operativo e não ao nível de todo o ambiente da máquina. Desta forma são mais “leves” que as VM, já que requerem apenas as configurações da aplicação e respectivas dependências.

*Containers* podem correr em diferentes máquinas, ou seja, são transportáveis. É possível partilhar *containers* entre computadores, mesmo com sistemas operativos diferentes.

Um *container* é criado a partir de uma **image** (imagem), que de um modo geral representa um sistema de arquivos com todos as dependências necessárias para criar o *container*. É possível criar *containers* a partir de imagens armazenadas em repositórios públicos ou criar uma nova imagem do zero a partir de um ficheiro de texto.

Todo este processo de *containerização* é garantido através de um **Container Engine** que gere todos os contentores na máquina. Uma das tecnologias mais utilizadas para o uso de containers é a plataforma *open-source* **Docker**, que será utilizado neste trabalho.

<br>

### Por que são usados *containers*?

Os contentores permitem simular o contexto para o qual uma aplicação vai ser executada e aí testar a aplicação nesse ambiente. Os contentores são muito úteis para testar dependências específicas (e não fundamentais) como uma biblioteca particular ou uma base de dados específica.

São muito úteis quando se aplica, por exemplo, uma arquitectura baseada em *microservices*, pois como os componentes da aplicação são desenvolvidos de forma independente, podemos mais facilmente testar individualmente partes desse sistema, que habitualmente utilizam diferentes tecnologias.

<br>

### O que é e para que serve o Docker?

O Docker é uma plataforma *open-source* que facilita a criação e gestão de *containers*. Permite fazer o deployment de uma aplicação de forma automatizada dentro de um *container* com ambiente Linux, visto utilizar funcionalidades do kernel Linux.

Vejamos agora alguns conceitos importantes no Docker:

- **Dockerfile** - É um ficheiro de texto que contém todos os comandos para a criação de uma `image`. Nele são descritas todas as instruções e características da `image` a ser gerada. Através de um `Dockerfile` é possível fazer um build de forma automatizada, que executará todos os comandos e instruções definidas.


- **Docker image** - É um ficheiro que define as configurações de instância de um *container*. Uma imagem é criada através das instruções definidas num `Dockerfile`. Como já foi referido, é possível criar imagens a partir de repositórios públicos ou escrevendo um `Dockerfile`;


- **Docker container** - É uma instância gerada através das configurações do docker image. À semelhança das máquinas virtuais, podemos executar `create`, `start`, `stop`, `delete` nos *containers* criados. É também possível configurar a rede ou acrescentar armazenamento ao *container*. Quando o *container* é removido, todas estas alterações são perdidas.

<br>

Antes de se avançar para o relatório técnico, apresentam-se alguns comandos habitualmente utilizados em **Docker**:


| Comando  | Descrição                                                  |
|:-----------:|:-------------:
| *docker info* | Informações sobre sistema e docker instalado      |   
| *docker images* | Lista de *images* locais |
| *docker pull [image]* | Obter uma *image* do Docker Hub |
| *docker run [image]* | Inicia um *container* docker |
| *docker ps* | Lista os container em modo *run* |
| *docker exec -it [container ID] [command]* | Executa um comando num *container* |
| *docker stop [container ID]* | Parar um *container* em modo *run* |
| *docker rm [container ID]* | Remover um *container* |
| *docker rmi [image]* | Remover uma *image* |
| *sudo docker run --name [container name] -i -t [image] [command] [arguments]* | Inicia um *container* a partir de uma *image* e executa um comando assim que o *container* entra em modo *run*, usando determinados argumentos |
| *docker build .* | Criar uma *image* a partir de um Dockerfile |

<br>

Um container pode ter vários estados. Esses estados podem ser definidos através dos seguintes comandos: `docker pause/unpause/start/restart/stop/run/kill [container id]`.


É também possível executar alguns comandos no Dockerfile, aquando da instanciação do `container`. Apresentam-se alguns:

| Comando  | Descrição                                                  |
|:-----------:|:-------------:
| ADD | Copia ficheiros a partir do host para o *container*. A fonte pode ser URL para download de ficheiros |
| COPY | Tem a mesma função de ADD, mas não suporta URL |
| CMD | Executa um comando quando o *container* é instanciado |
| ENTRYPOINT | Aplicação *default* a ser executada pelo *container* |
| ENV | Define variáveis de ambiente |
| EXPOSE | Expõe um *port* na máquina *host* |
| FROM | Indica a *image* base |
| RUN | Executa um comando durante a build da *image* |
| VOLUME | Permite o acesso a um directório na máquina *host* |
| WORKDIR | Mudar o *working directory* no *container* |

<br>


## Relatório técnico - Criar *containers* com Docker


Neste **CA4 Parte I** vamos criar uma `docker image` para poder criar um `container` para executar a aplicação ChatApp utilizada no CA2 (também disponível em `(https://bitbucket.org/luisnogueira/gradle_basic_demo/)`. O trabalho divide-se em **Parte A** e **Parte B**. Na primeira versão o objectivo é definir o *build* dentro do `Dockerfile`. Na segunda, o *build* deverá ser feito na máquina, copiando o respectivo ficheiro `jar` para o `Dockerfile`.

Mas antes é necessário fazer algumas configurações iniciais

 **1. Instalar o WSL2**

Dado que o sistema operativo a utilizar neste trabalho é o Windows 10 Home, para utilizar a versão Desktop do Docker (para Windows), é necessário instalar primeiro o WSL	2 (Windows Subsystem for Linux), seguindo as indicações em `https://docs.microsoft.com/en-us/windows/wsl/install`

**2. Instalar o Docker Desktop** Instalado o WSL2, podemos agora instalar o **Docker Desktop** disponível em `https://docs.docker.com/desktop/windows/install/`

<br>

### Parte A

### 1. Criar Docker image e Container

**1.1.** Na pasta do repositório `CA4\Part_1A`, criamos um ficheiro `Dockerfile`.

**1.2** Abrimos o ficheiro no IDE, no nosso caso no o `IntelliJ IDEA`, e definimos todas as dependências e comandos necessários para a criação da `image` pretendida:

<br>

```
# Docker image
FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

# install all dependencies and clean temporary files generated by apt-get
RUN apt-get update -y && apt-get install -y git \
    openjdk-11-jdk-headless \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# set the working directory
ENV DIR=/temp-build
WORKDIR $DIR

#clone the repo with the application
RUN git clone https://hugomorgadinho@bitbucket.org/hugomorgadinho/devops-21-22-lmn-1211766.git

#set directory to the application folder
WORKDIR $DIR/devops-21-22-lmn-1211766/CA2/Part_1/gradle_basic_demo

# Conceding permission to use gradle wrapper
RUN chmod u+x gradlew
# build the app and copy jar file to the current directory
RUN ./gradlew clean build && cp build/libs/basic_demo-0.1.0.jar .
# clean the build generated
RUN ./gradlew clean

EXPOSE 59001

#Execute the server in the container
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

<br>

**1.3.** A partir deste `Dockerfile` podemos agora criar uma `image`. Para isso, executamos o comando `docker build -t ca4-part1-a .`

**1.4.** De seguida, a partir da `image` construída, geramos um `container`: `docker run --name devops-ca4-1a -p 59001:59001 -d ca4-part1-a`

Para compreender o comando anterior: O`container` terá o nome `devops-ca4-1a`, será exposto no `port 59001`, e utiliza a imagem `ca4-part1-a`.

<br>

### 2. Tag *image* para publicar no Docker Hub

**2.1.** Primeiro é necessário criar conta em Docker Hub em `https://hub.docker.com/`.


**2.2** Criada a conta, no terminal, executamos `docker login`.

**2.3.** Com o login feito, executamos `docker tag ca4-part1-a hugoamp/ca4-part1-a:version1.0`

**2.4.** Por fim, fazemos push: `docker push hugoamp/ca4-part1-a:version1.0`

<br>

### 3. Testar a aplicação ChatApp

**3.1.** No `Dockerfile` definimos o comando para executar a aplicação no lado do servidor. Confirmamos que o servidor está a "correr" através do terminal do `container`, no GUI do Docker.

**3.2.** Na nossa máquina, no directório da aplicação, executamos `./gradlew runClient`. Uma janela com a aplicação fica visível, permitindo-nos testar a app.

**3.3** Fechamos a janela e confirmamos no terminal do `container` que o utilizador saiu da sessão.

<br>

### Parte B

### 4. Executar a aplicação com nova *image*

**4.1.** Na pasta do repositório CA4\Part_1B, criamos novamente um ficheiro Dockerfile.

**4.2** Abrimos o ficheiro no IDE e definimos as dependências e comandos necessários:

<br>

```
# Docker image
FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

# install all dependencies
RUN apt-get update -y
RUN apt-get install openjdk-11-jdk-headless -y

# set the working directory
ENV DIR=/temp-build
WORKDIR $DIR

#set directory to the application folder
WORKDIR $DIR/devops-21-22-lmn-1211766/CA2/Part_1/gradle_basic_demo

# Copy the jar file into the container
COPY /build/libs/basic_demo-0.1.0.jar .

EXPOSE 59001

#Execute the server in the container
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

<br>

**4.3.** Executamos o comando `comando : docker build -t ca4-part1-b .` para criar uma nova `image` a partir deste último `Dockerfile`

**4.4.** Geramos então o container: `docker run --name devops-ca4-1b -p 59001:59001 -d ca4-part1-b`

**4.5.** Confirmamos que o servidor está a "correr" através do terminal do container, no GUI do Docker.

**4.6.** Voltamos a executar a aplicação no lado do cliente (na máquina *host*): `./gradlew runClient`. A janela do chat fica visível, permitindo-nos testar a app.

**4.7.** Fechamos a janela e confirmamos no terminal do container que o utilizador saiu da sessão.

<br>

### 5. Tag *image* para publicar no Docker Hub

Desta vez optou-se por fazer a `tag` da imagem no final, depois de testar a aplicação.

**5.1.** Fazemos novamente login `docker login`, no entanto, desta vez não é necessário colocar *username* nem *password*.

**5.2.** Assinalamos a tag na imagem: `docker tag ca4-part1-b hugoamp/ca4-part1-b:version2.0`

**5.3.** Por fim, fazemos o push: `docker push hugoamp/ca4-part1-b:version2.0`




