# Class Assignment 2

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

### O que é uma building tool?

Building tool é um sistema que permite automatizar um conjunto de tarefas realizadas com frequência por qualquer software developer. Tarefas essas que podem ser: compilar *source code* para *machine code* (código binário), realizar testes, *packaging* ou *deployment* de uma aplicação, utilizar bibliotecas externas, entre outras.

A grande vantagem destas ferramentas é libertar o developer deste tipo de tarefas repetitivas e monótonas. Com uma building tool, passa a ser possível executar todas estas acções de forma automatizada e organizada, permitindo assim ao developer focar-se no desenvolvimento da sua aplicação.

<br>

### O que é o Gradle?

Gradle é uma building tool *open-source* muito utilizada no desenvolvimento de aplicações JVM. Ainda que compatível com linguagens como C / C++ ou JavaScript, é sobretudo no ambiente Java que mais se destaca.

Foi lançado em 2007, sucedendo os competidores directos Apache Ant e Maven. Apesar de desempenharem praticamente as mesmas funções, fazem-no de maneira muito diferente. A grande diferença do Gradle relativamente ao Ant e Maven é sobretudo ser baseado em linguagens dinâmicas, como Groovy ou Kotlin, ao contrário dos anteriores que recorrem a uma linguagem markup, XML, para  configuração de projectos.

O Gradle perimite a integração com diversos IDE’s e ferramentas de desenvolvimento, como por exemplo IntelliJ, Eclipse, Android Studio ou Jenkins.

#### *Projects* e *tasks*

O Gradle funciona com base em *projects* e *tasks*. Cada *build* pode conter um ou vários *projects* e cada um desses contém várias *tasks*. Uma *task* representa uma acção a executar durante um build. Pode ser compilação a de código-fonte ou copiar dados de um directório para outro. É também possível declarar dependências numa task.

Através de tasks podemos definir toda a acção de um build.


<br>

## CA2 - Parte 1

<br>

### Preparação inicial

**1.** Começamos agora a desenvolver o trabalho propriamente dito relativo ao CA2. Para isso, no repositório individual de DevOps criamos um novo directório para este trabalho. Para facilitar a organização, criamos aí dentro dois directórios. Um para Part_1 e outro para Part_2. Utilizamos os seguintes comandos:

1.1 - Criar o directório “CA2”: ```mkdir CA2```


1.2 - Movemo-nos para esse directório: ``cd CA2``


1.3 - Criamos os directórios Part_1 e Part_2: ``mkdir Part_1 Part_2``


1.4 - Movemo-nos para a Part_1: ``cd Part_1``

**2.** Nessa pasta, clonamos a aplicação disponibilizada: ``git clone https://hugomorgadinho@bitbucket.org/luisnogueira/gradle_basic_demo.git``


**3.** Ainda nessa pasta, criamos um ficheiro *readme*, onde será desenvolvido o relatório técnico: ``touch readme.md``

### Realização do trabalho

Já com a aplicação clonada no repositório, seguimos as indicações do ficheiro *readme* disponibilizado e experimentamos a aplicação Chat App. Esta aplicação simula uma chat room, tanto do lado do servidor como do lado do cliente.

**1.** O próximo passo consiste em criar uma nova *task* que permite executar o servidor.
No IDE, abrimos o projecto **gradle_basic_demo** e de seguida o ficheiro **build.gradle**. Aí, criamos a nova *task* com o nome **runServer**, na qual atribuímos como mainClass a classe ChatServerApp. Em args deixamos apenas como parâmetro o port “59001”. Editamos a descrição referindo a finalidade desta task. Na seguinte imagem observamos o resultado final:

```
task runServer(type:JavaExec, dependsOn: classes){
group = "DevOps"
description = "Runs server"

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'
}
```

**2.** Criada esta nova task, experimentamos no terminal executando o comando ``./grandlew runServer``. Como esperado, o servidor corre.

**3.** De seguida, adicionamos um teste unitário ao projecto, usando o exemplo disponibilizado no enunciado deste exercício. De maneira a podermos executar o teste, editamos o script gradle, acrescentando nas dependências de build.gradle a versão **4.12 do jUnit**. Isto é, ``testImplementation 'junit:junit:4.12'``. Vejamos o resultado na imagem seguinte:

```
dependencies {
    // Use Apache Log4J for logging
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    testImplementation 'junit:junit:4.12'
}
```

**4.** Para confirmar que temos acesso a esta **task test**, utilizamos o comando ``./gradlew tasks``, o qual depois de executado apresenta uma lista com todas as tasks disponíveis. Como esperado, a task test já se encontra na lista. 


Executamos então esta task através do comando ``./gradlew test`` e o build é feito com sucesso.

**5.** A seguir adicionamos uma nova tarefa que tem como finalidade garantir um backup de todas as fontes da aplicação. Nesta task intitulada **copySrc**, do tipo copy (implementa a interface CopySpec para especificar o que copiar), define-se o directório **src** como directório de conteúdo a copiar.  

Como directório de destino definimos **backup**. Não será necessário criar manualmente este directório. Vejamos a implementação desta task:

```
task copySrc(type: Copy) {
    from 'src'
    into 'backup'
}
```

Executamos a task recorrendo ao comando ``./gradlew copySrc``. O build é feito com sucesso.

Quando a task é executada, o directório é criado automaticamente, armazenando do seu interior cópias dos directórios **main** e **test**.


**6.** Vamos agora criar uma nova task, desta vez o objectivo é armazenar as fontes da aplicação num ficheiro zip. Damos como nome a esta task **archiveSrc**, definimos o tipo Zip, e damos valores às propriedades. Em archiveFileName definimos como nome **src.zip**. Definimos from como **src** e destinationDirectory **file(‘backup’)**. Vejamos a implementação desta task:

```
task archiveSrc(type: Zip) {
    archiveFileName  = 'src.zip'
    from 'src'
    destinationDirectory = file('backup')
}
```

Executamos no terminal a task: ``./gradlew archiveSrc``. O build é feito com sucesso e um ficheiro zip com o nome ‘src’ é criado no directório backup.

