# Class Assignment 2

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte 2 - Alternativa com Maven

<br>

### O que é o Maven?

Maven é outra *building tool* muito conhecida e utilizada sobretudo no contexto de desenvolvimento de projectos Java. Foi desenvolvida pelo Apache Group e à semelhança do *Gradle*, é uma ferramenta *open source*, capaz de simplificar e automatizar  variadíssimas tarefas, como por exemplo: “construir” (build) projectos, gerir dependências, documentação, relatórios de testes unitários, entre outras.


Utiliza um modelo Project Object Model (POM) que, de um modo geral, permite gerir todo o projecto através de ficheiro *xml*. Neste ficheiro, que é designado como `pom.xml`, são incluídas todas as dependências ou módulos de componentes externas, ordens de compilação ou *plugins* necessários.

O Maven é construído numa arquitectura baseada em *plugins*. Alguns destes *plugins* vêm já pré-definidos na ferramenta, mas é sempre possível redefinir ou criar novas funcionalidades, o que facilita muito o processo de desenvolvimento de um projecto.


### Principais diferenças entre Gradle e Maven

Existem algumas diferenças fundamentais na maneira como Gradle e Maven encaram o processo de *build*.

O Gradle baseia-se numa gestão de tarefas (*tasks*) - as quais desempenham determinada função, enquanto o Maven baseia-se mais num modelo fixo e linear de objectivos (*goals*). *Goals*, em Maven, são atribuídos ao projecto e são responsáveis por cumprir determinadas funções.

Existem diferenças a vários níveis, nomeadamente em termos de *performance*, *uso* ou *gestão de dependências*.

| | Gradle                                                                         | Maven | 
|--------------------|--------------------------------------------------------------------------------|----------------------------------------|
|**Configuração** | Estrutura do projecto definida através de *Domain Specific-Language* (DSL). Utiliza linguagens de programação, como por exemplo, Groovy  | Estrutura de projecto definida através de  XML (linguagem markup)    
|**Performance**| Foca-se apenas nas tarefas necessárias para o processo e evita a constante compilação de classes e testes já compilados. Gradle Deamon torna o build mais rápido |   Não armazena build-cache, o que torna o build mais lento    
|**Utilização**| Leitura e manutenção simples, visto recorrer a uma linguagem de programação | Recorre a linguagem *markup*, ficheiro de configuração torna-se mais "estático" e por consequência longo  
|**Foco**| Desenvolvimento de projectos através da adição de novas funcionalidades  | Desenvolvimento de projectos centra-se mais numa idea de tempo limitado (deadlines)         
|**Linguagem suportadas**| Suporta linguagens como Java, C, C++, and Groovy | Suporta linguagens como Scala, C#, and Ruby    **

<br>

### Implementação 

Vamos então desenvolver a parte 2 do CA2, desta vez recorrendo ao **Maven**.

**1.** À semelhança do que foi feito anteriormente, criamos um directório novo, designado por **Part_2_Alternative**, e ainda um novo ficheiro readme. O trabalho será desenvolvido numa branch com o nome **tut-basic-maven-ca2-alternative**.

**2.** Seguindo novamente o enunciado, iniciamos um novo projecto spring boot, desta vez com Maven.

**3.** Voltamos a apagar o directório src do projecto gerado, substituindo por uma cópia da pasta src (e restantes subdirectórios) relativo ao tutorial basic.

**4** Copiamos ainda os ficheiros *webpack.config.js* e *package.json* do tutorial basic para o actual projecto. Voltamos a apagar a pasta *build*, onde são armazenados os registos build já realizados, localizada em `src/main/resources/static/built/`.

**5.** Experimentamos a aplicação, executando o comando `./mvnw springboot:run` . Como não existe nenhum plugin relativo ao frontend, a página localizada em **localhost 8080** estará vazia.

**6.** Vamos então adicionar um plugin que permita lidar com o frontend. Como estamos a utilizar Maven, recorremos ao **frontend-maven-plugin** retirado do seguinte repositório: `https://github.com/eirslett/frontend-maven-plugin`.


**7.** Para ser possível executar o webpack , actualizamos o objecto *scripts* no package.json, colocando as seguintes linhas de código

```
"scripts": {
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
},

```


**8.** Fazemos novamente build do projecto, utilizando o comando `./mvnw install`.


**9.** A seguir corremos de novo a aplicação: `./mvnw springboot:run`. Agora já será possível visualizar o frontend.


**10.** Criamos um novo plugin com o objectivo de copiar os ficheiros jar gerados. O directório *target* (onde se encontram os ficheiros Jar, em Maven) é definida como fonte da cópia. O destino será uma pasta **dist**. Vejamos a implementação deste plugin:


```
<plugin>
  <artifactId>maven-resources-plugin</artifactId>
  <version>3.2.0</version>
  <executions>
     <execution>
        <id>copy-jar-files</id>
        <phase>generate-sources</phase>
        <goals>
           <goal>copy-resources</goal>
        </goals>
        <configuration>
           <outputDirectory>dist</outputDirectory>
           <resources>
              <resource>
                 <directory>target</directory>
                 <includes>
                    <include>*.jar</include>
                 </includes>
              </resource>
           </resources>
        </configuration>
     </execution>
  </executions>
</plugin>

```

**11** Redefinimos agora o plugin clean para eliminar automaticamente os ficheiros gerados pelo webpack, normalmente armazenados em `src/resources/main/static/built/`. Implementamos o plugin da seguinte maneira:

```
<plugin>
  <artifactId>maven-clean-plugin</artifactId>
  <version>3.2.0</version>
  <configuration>
     <filesets>
        <fileset>
           <directory>src/main/resources/static/built</directory>
           <followSymlinks>false</followSymlinks>
        </fileset>
     </filesets>
  </configuration>
</plugin>

```


**12** Agora podemos testar estes dois plugins.


**12.1** Primeiro a executa-se a operação de cópia, fazendo build: `./mvnw install`. Como pretendido, é feita uma cópia dos ficheiros Jar para uma nova pasta *dist*.


**12.2** De seguida, executamos o plugin de clean: `./mvnw clean`
. Confirmamos com sucesso que a pasta *build*, é eliminada de `src/main/resources/static/`

<br>

### Considerações finais

Terminado este segundo Class Assignment, onde foi possível pôr em prática os conhecimentos adquiridos sobre *building tools*, primeiro utilizando o Gradle, depois Maven, posso agora afirmar que são notórias, na prática, as diferenças entre estas duas ferramentas. De um modo geral, as pesquisas feitas antes da realização do trabalho sobre as diferenças entre estas duas building tools estão de acordo com a minha experiência. A configuração do projecto é muito mais simples e intuitiva em Gradle. Essa simplicidade facilita muito na leitura e na manutenção, sobretudo para quem é iniciante.

Em Gradle, é possível definir uma dependência numa única linha, por exemplo. Ou criar uma task em poucas linhas. Ora, essas mesmas tarefas exigem muito mais “código” em Maven. Se em projectos pequenos isto pode não ser um grande problema, em projectos de maior dimensão, com muitas dependências e plugins, pode tornar-se bem mais caótico, dificultando muito a gestão do projecto.

Por se basear numa linguagem de programação, em Gradle, a gestão é muito mais dinâmica. E podemos executar várias tarefas em paralelo.

Neste sentido, senti clara vantagem do Gradle na configuração do projecto. Também a nível de comandos, o Gradle pareceu–me mais intuitivo e versátil.

Uma nota também nível de documentação. Ambas disponibilizam documentação oficial muito útil e fácil de consultar. Para além disso, existe também muita documentação não oficial que ajuda na resolução de possíveis problemas.

Este trabalho deu-me a conhecer o potencial de uma building tool. Na verdade, aquilo que permite fazer é bem mais do que “simples” compilação de *source code* para *machine code*. Com conhecimento e experiência, podemos configurar e planear inúmeras funcionalidades, úteis no processo de desenvolvimento de software. Consciente de que o conhecimento adquirido ainda é muito ténuo, fico motivado para explorar ainda mais este tipo de ferramentas.
