# Class Assignment 2

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte 2

<br>

**1.** Começamos agora a desenvolver a segunda parte do CA2. No directório **Part_2** criamos um novo ficheiro readme. Criamos também uma nova branch, com o nome **tut-basic-gradle**, para desenvolver esta segunda parte do trabalho.


**2** Como solicitado no enunciado, inicia-se um novo projecto gradle spring boot com as dependências sugeridas. No directório Part_2, extraímos o ficheiro com a aplicação (ainda vazia). Voltamos a conhecer as tasks disponibilizadas pelos plugins através do  comando `./gradlew tasks`.


**3** Uma vez que pretendemos utilizar o código do *tutorial basic* (idêntico ao usado na primeira aula de laboratório), apagamos o directório src (vazio) gerado neste projecto gradle spring boot e substituímos por uma cópia do directório src (e respectivos subdirectórios) do tutorial basic da referida aula.


**4** Para além do directório src, copiamos ainda os ficheiros **webpack.config.js** e **package.json** para o actual projecto. Depois, é ainda necessário apagar a pasta *build* (onde são armazenados os *builds artifacts* realizados anteriormente), localizada em `src/main/resources/static/built/`, visto que será posteriormente gerada através do JavaScript pela webpack tool.


**4** Podemos agora experimentar a aplicação, executando o comando `./gradlew bootRun`. Tal como previsto, a página localizada no **localhost 8080** está vazia, visto faltarem ainda os plugins relativos ao frontend.


**5** Assim sendo, adicionamos o referido plugin, neste caso o **org.siouan.frontend**, ao nosso projecto, para que o gradle possa lidar com o frontend. Para isso, adicionamos a linha `id "org.siouan.frontend-jdk11" version "6.0.0"` aos plugins em build.gradle. A lista de plugins fica então assim:

```
plugins {
  id 'org.springframework.boot' version '2.6.6'
  id 'io.spring.dependency-management' version '1.0.11.RELEASE'
  id 'java'
  id "org.siouan.frontend-jdk11" version "6.0.0"
}
```



**6** Para configurar o plugin anterior, adicionamos o seguinte excerto de código ao build.gradle:

```
frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}

```
**7** Para ser possível executar o webpack , actualizamos o objecto *scripts* no package.json, colocando as seguintes linhas de código

```
"scripts": {
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
},

```

**8** Agora podemos executar o build, que executará também as tasks relativas ao frontend. Utilizamos então o comando `./gradlew build`.


**9** De seguida, executamos de novo a aplicação: `./gradlew bootRun`


**10** Adicionamos uma nova tarefa que tem como objectivo copiar os ficheiros jar gerados. Nesta task, cujo nome é **copyJar**, definimos a pasta **build/libs** (onde estão localizados os ficheiros jar) como directório de conteúdo a copiar e colocamos como destino **dist**. À semelhança do que acontece na Part 1, não será necessário criar manualmente este directório, pois será automaticamente criado aquando da execução da task. Vejamos a implementação desta task:

```
task copyJar(type: Copy) {
  from 'build/libs'
  into 'dist'
}

```

Testamos agora a nova task executando o comando `./gradlew copyJar`.


**11** Criamos agora uma nova tarefa que tem como finalidade eliminar os ficheiros gerados pelo webpack. Estes ficheiros são normalmente armazenados em `src/resources/main/static/built/`, como já referido. Pretende-se que esta tarefa seja executada automaticamente antes da execução da **task clean**. Neste sentido, implementamos a task da seguinte maneira:

```
task delete(type: Delete) {
  delete 'src/main/resources/static/built'
}

clean.dependsOn delete

```

**12** Finalizado este trabalho, testamos as duas tasks gradle.


**12.1** Primeiro a executa-se a tarefa de cópia, utilizando o comando `./gradlew copyJar`. Como desejando, é feita uma cópia dos ficheiros Jar para uma nova pasta *dist*.


**12.2** De seguida, executamos o build para depois poder executar a tarefa de delete:
Executa-se o comando `./gradlew build`
E de seguida, então, testamos a tarefa, utilizando `./gradlew clean`


Confirmamos com sucesso que a pasta *build*, é eliminada de `src/main/resources/static/`


Realizada a implementação pretendida, pode agora fazer-se o merge para a main branch.
