import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void checkEmployeeFirstNameWithSuccess(){
        Employee employee = new Employee("Arlindo", "Cunha", "Departamento de Manutenção", "electricista", 12, "email@email.com");
        String result = employee.getFirstName();
        assertEquals("Arlindo", result);
    }

    @Test
    void createInvalidEmployeeWithEmptyFirstName() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> new Employee("", "Cunha", "Departamento de Manutenção", "electricista", 12, "email@email.com" ));
        Assertions.assertEquals("First name and last name cannot be empty", thrown.getMessage());
    }


    @Test
    void createInvalidEmployeeWithEmptyDescription() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> new Employee("Arlindo", "Cunha", "", "electricista", 12, "email@email.com"));
        Assertions.assertEquals("Please insert a description", thrown.getMessage());
    }

    @Test
    void createInvalidEmployeeWithEmptyJobTitle() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> new Employee("Arlindo", "Cunha", "Departamento de Manutenção", "", 12, "email@email.com"));
        Assertions.assertEquals("Please insert a job title", thrown.getMessage());
    }

    @Test
    void createInvalidEmployeeWithNegativeNumberOfJobYears() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> new Employee("Arlindo", "Cunha", "Departamento de Manutenção", "electricista", -1, "email@email.com"));
        Assertions.assertEquals("Invalid number. Please insert a valid number", thrown.getMessage());
    }

    @Test
    void createInvalidEmployeeWithInvalidEmailBecauseNotUsingAt() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> new Employee("Arlindo", "Cunha", "Departamento de Manutenção", "electricista", -1, "email.com"));
        Assertions.assertEquals("Please insert a valid email", thrown.getMessage());
    }

    @Test
    void createInvalidEmployeeWithInvalidEmailBecaudeNotUsingDot() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> new Employee("Arlindo", "Cunha", "Departamento de Manutenção", "electricista", -1, "email@email"));
        Assertions.assertEquals("Please insert a valid email", thrown.getMessage());
    }



}