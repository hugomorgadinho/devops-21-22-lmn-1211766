

# Class Assignment 1
**Aluno**: Hugo Pereira

**N.º de aluno** 1211766

<br>



### O que é o Git?

Git é um sistema de controlo de versões (Version Control System - VCS), gratuito e open source, concebido para gestão de projectos de pequena e larga escala. É sobretudo utilizado na indústria tecnológica, mas pode também ser utilizado em projectos de qualquer natureza. Foi inicialmente criado por Linus Torvalds e por outros developers da sua equipa, enquanto trabalhavam no kernel do Linux. Na área IT, é de longe o sistema mais conhecido e utilizado. Num inquérito realizado em 2018 na plataforma Stack Overflow, quase 90% dos participantes diziam integrar o Git nos seus projectos. 

À semelhança de outros VCS, o Git permite acompanhar e gerir as várias alterações de um projecto ao longo do tempo. É muito útil no contexto de desenvolvimento em equipa, pois permite integrar de forma muito rápida e simples diferentes versões desenvolvidas. Desta maneira, as equipas podem trabalhar simultaneamente em diferentes funcionalidades de uma aplicação, o que permite uma maior eficácia e fluidez no desenvolvimento de software.

### Git: Arquitectura e design - como funciona o git?

**Sistema de Controlo de Versões distribuído** - Cada utilizador possui um repositório independente. Quando necessário, através das operações **push** e **pull**, as actualizações são enviadas ou recebidas de ou para outro repositório remoto. A ideia é não existir um repositório "principal", ainda que normalmente exista um repositório central para facilitar o fluxo de trabalho.

**Fluxo de trabalho** Ao contrário da maior parte dos SCV, que funcionam a dois níveis, o Git têm uma arquitectura de fluxo de trabalho a três níveis:

- **Working directory** - A área de trabalho local, criada quando o Git é inicializado e onde desenvolvemos o nosso trabalho;


- **Staging Area** Esta é a área que habitualmente não existe em outros VCS. No fundo, é um espaço temporário, onde armazenamos os ficheiros editados e que serão adicionados no repositório num próximo commit;


- **Git Directory (Repositório)** - A área onde as alterações ao projecto são armazenadas. Depois de colocados os ficheiros na Staging Area, Git aramazena um snapshot da versão do nosso trabalho no repositório;

<br>

![Git_Working_tree](https://git-scm.com/book/en/v2/images/areas.png)

**Branch** O Git permite trabalhar em diferentes versões de um projecto. Isto acontece também graças a um sistema de "branches". Desta maneira, podemos desenvolver uma nova funcionalidade numa branch nova e deixar na branch principal a versão estável. A imagem seguinte ilustra esta possibiidade:

![Git_Different_History](https://kalkus.dev/wp-content/uploads/2020/05/git_merge_rebase.png)


**Commit** - Um commit é um snapshot de um determinado momento do nosso projecto. Por outras palavras, um registo do trabalho desenvolvido até certo momento.

**Pull** - Consiste no acto de copiar as alterações guardadas num repositório remoto para o nosso repositório local;

**Push** - Permite enviar as alterações desenvolvidas localmente para um repositório remoto;

**Head Pointer** - De um modo geral, permite saber a nossa posição no repositório, aponta sempre para o último commit da branch em que nos encontramos.

### Comandos de ajuda e configurações iniciais

Vamos agora ver na prática como funciona o Git. Começamos pelos comandos de apoio e de configuração inicial. Passamos de seguida para o trabalho do CA1.
<br>

#### Git Help
**Função** : Pesquisar comandos Git e respectiva função:

- **git help** apresenta uma lista dos comandos Git mais utilizados e respectiva descrição.

- **git help [comando]**  apresenta descrição da funcionalidade específica do comando introduzido. No caso de git add, por exemplo, o comando a introduzir será: git help add

<br>

#### Configurações Git
Configurar informações do utilizador e modo de como utilizar o Git

- **git config -l** permite visualizar configurações de conta

- **git config --global user.name "Hugo Pereira [1211766]"** configura o nome do utilizador(username)

- **git config --global user.email 1211766@isep.ipp.pt** configura o e-mail do utilizador

- **git config --global core.editor nano** configura o editor de texto a utilizar no Git, neste caso para o editor Nano

<br>

### CA1 - Parte 1

<br>

**1** Abrir linha de comandos GIT BASH HERE no directório do repositório;

**2** Criar directório "CA1" através do comando **mkdir CA1**

**3** Mover par o directório CA1 através do comando **cd CA1/**

**4** Criar ficheiro "readme.txt" através do comando **touch readme.txt**

**5** Commit com a criação do ficheiro readme:


 - **5.1** Colocar o ficheiro na Staging Area: **git add readme.txt**


 - **5.2** Realizar commit: **git commit -m "create readme file"**




**6** Remover ficheiro "readme.txt", visto que o ficheiro correcto deverá ser "README.md": **git rm readme.txt**

**7** Criar de novo ficheiro readme, desta vez "README.md": **touch README.md**

**8** Colocar ficheiro na Staging Area: **git add .**

**9** Redefinir commit anterior para a criação de novo ficheiro "README.md": **git commit --amend -m "Create new README.md file"**

**10** Confirmar alteração da descrição do commit através de **git log** (comando que permite consultar dados sobre commits realizados)

**11** Copiar código do Tutorial React.js and Spring Data REST Application para o directório CA1: **cp -R tut-react-and-spring-data-rest CA1/**

**12** Commit com cópia de aplicação Tutorial React.js and Spring Data REST Application para pasta CA1
		
- **12.1** Colocar ficheiro na Staging Area: **git add .**
		

- **12.2** Realizar commit: **git commit -m "Copy the Tutorial React.js and Spring Data REST Application into CA1 directory (resolve #3)"**

<br>

**13** Push para o repositório remoto (Bitbucket): **git push**

**14** Colocar tag com versão da aplicação (v.1.1.0): **git tag v1.1.0**

**15** Push para o servidor para atribuição de tag: **git push origin v1.1.0**

**16** Implementar na aplicação nova funcionalidade e fazer respectivos testes: adicionar novo campo com anos do funcionário na empresa;

**17** Commit da implementação da nova funcionalidade:

- **17.1** Colocar ficheiro na Staging Area: **git add .**
 

- **17.2** Realizar commit: **git commit -m "Implement and test new feature to the app (resolve #4)"**

<br>

**18** Push para o repositório remoto com a actualização do trabalho feito (Bitbucket): **git push**

**19** Colocar tag com versão da aplicação (v1.2.0): **git tag -a v1.2.0 -m "nova versão v1.2.0"**

**20** Push para o servidor com tag da nova versão: **git push origin v1.2.0**

**21** Confirmar estado de Working Directory e Staging Area: **git status**

**22** Repetir Commit e Push depois de verificar através do comando **git status** que afinal ainda existem alterações no Working Directory. O comando anterior permite precisamente consultar o estado de Working Directory e Staging Area:


- **22.1** Colocar alterações ao ficheiro na Staging Area: **git add .**
 

- **22.2** Fazer commit : **git commit -m "Implement and test new feature to the app (resolve #4)"**

<br>


**23** Remover localmente tag "v1.2.0" de commit anterior que não contem as actualizações do trabalho: **git tag -d v1.2.0**
		
**24** Remover remotamente (no Bitbucket) tag "v1.2.0" de commit anterior que não contem as actualizações do trabalho: **git push --delete origin v1.2.0**

**25** Repetir push para o servidor com tag da nova versão, associando ao commit mais recente e que contem as alterações: **git push origin v1.2.0**

**26** Escrever no markdown todas as acções para o Class Assignment 1 (CA1) e respectivos comandos git;
	
**27** Commit da actualização de ficheiro markdown:

- **27.1** Colocar alterações no ficheiro na Staging Area: **git add .**
 

- **27.2** Realizar commit : **git commit -m "create markdown file with all CA1 working stepes (resolve #5)"**

<br>

**28** Push para o repositório remoto com a actualização do trabalho feito (Bitbucket): **git push**.

**29** Colocar tag no último commit do trabalho desenvolvido em CA1: **git tag -a ca1-part1 -m "versão do trabalho ca1-part1"**.

**30** Push para o repositório remoto(Bitbucket): **git push origin ca1-part1**.

<br>

---------------------------------

### CA1 - Parte 2

**1** Pretende-se adicionar uma nova funcionalidade à aplicação, neste caso um campo para e-mail de funcionário.
Para isso, é importante criar uma nova branch para a implentação da nova feature, deixando a master branch para
a versão "estável" do trabalho. Assim sendo, vamos então criar a uma nova branch "email-field" através do comando **git branch email-field**.

**2** Confirmar a criação da nova branch através do comando **git branch**, que permite precisamente visualizar uma lista de branches no nosso repositório e ainda conhecer a nossa localização (posição onde se encontra o *). Este comando é sinónimo de **git branch --list**.

**3** Mover para a nova branch, usando o comando **git checkout email-field**. Existe uma alternativa mais recente para este comando, o comando **git switch email-field**, que produz o mesmo efeito.

**4** Implementar na aplicação nova funcionalidade relativa a e-mail de funcionário e fazer respectivos testes.

**5** Depois de implementada e testada a nova funcionalidade, fazemos o push da nova branch para o repositório remoto (Bitbucket), realizando os seguintes passos:


- **5.1** Colocar ficheiro na Staging Area: **git add .**


- **5.2** Realizar commit: **git commit -m "Implement and test new e-mail feature to the app (resolve #6)"**
 

-  **5.3** Mudar para a master branch: **git switch main**


- **5.4** Fazer push para remoto: **git push origin email-field**


**6** De maneira a podermos observar no GUI do repositório remoto o desvio (ou divergência) provocado pela nova branch, realizamos um commit na master branch:

- **6.1** Acrescentar uma linha de comentário no tutorial React.js and Spring

 
- **6.2** Fazer commit com a alteração: **git commit -a -m "update app"**


- **6.3** Fazer push para o remote: **git push**


**7** Feito o push para o remote (Bitbucket) da nova branch, vamos então fazer o merge para a master branch. Isto é, integramos na master branch o trabalho desenvolvido na branch email-field. Merge serve precisamente para isso: integrar o trabalho desenvolvido em duas branches idependentes numa única branch. Para isso, executamos os seguintes passos:

- **7.1** Garantir que estamos na master branch, a branch de destino do merge. Para isso, usamos o comando **git branch**. Caso não estejamos na branch master, executamos o comando **git switch master**


- **7.2** Fazer o merge: **git merge email-field**


- **7.3** Fazer o push para o remote: **git push origin**
 

**8** Feito o merge e push para o remote, colocamos a tag com a versão 1.3.0:

- **8.1** Primeiro, usando o comando **git tag v1.3.0 -m "nova versão 1.3"**


- **8.2** Push para o servidor com a nova tag: **git push origin v1.3.0**
 

**9** Vamos agora provocar uma situação de conflito num merge. O que significa isto? Significa que quando decidimos fazer o merge de duas branches, um ou vários dos conteúdos de um repositório apresentam registos diferentes em pelo menos uma linha. O sistema de controlo de versões tenta juntar estas diferentes versões de cada branch, mas como são "incompatíveis" (sobrepõem-se) não executa o pedido. Para concluir o merge, temos de decidir o que fica e o que sai na versão final.


- **9.1** Na master branch, onde nos encontramos, criamos um ficheiro html no directório CA1 com alguns parágrafos, usando o comando **nano index.html**. Fazemos commit.

 
- **9.2** Criamos a nova branch "conflict-merge" e movemo-nos para lá recorrendo aos comandos anteriormente utilizados no início da Parte II. 


- **9.3** Na nova branch editamos o ficheiro html com diferentes parágrafos. Colocamos as alterações na Staging Area e fazemos commit.

 
- **9.4** Regressamos à master branch e fazemos o push para o repositório remoto recorrendo aos comandos utilizados no ponto 5 desta Parte II do CA1

 
- **9.5** Na master branch, executamos agora o comando **git merge conflict-merge**

 
- **9.6** Na linha de comandos apresenta-se a mensagem de que existem conflitos e que o merge falhou. Antes do commit, temos de resolver os coflitos.

 
- **9.7** Resolvidos os conflitos, optando pelos parágrafos pretendidos, executamos git push para o repositório remoto.



**10** Feito o merge, podemos agora eliminar a "conflict-merge" branch. Primeiro eliminamos a nível local e de seguida no repositório remote:


- **10.1** Para eliminar localmente executa-se o comando **git branch -d conflict-merge** 


- **10.2** Já a nível remoto é necessário utilizar o comando **git push origin --delete conflict-merge**


**11** Pretende-se agora criar uma nova branch para resolução de bug, neste caso um bug relacionado com a validação de e-mail, a qual iremos chamar "fix-invalid-email". Desta maneira, voltamos a deixar a master branch com a versão estável da aplicação:


- **11.1** Executar o comando **git branch fix-invalid-email** para criar a nova branch


- **11.2** Mover para a nova branch: **git checkout fix-invalid-email**
 

**12** Resolver o bug da validação de e-mail na aplicação.


**13** Depois de resolvido o bug, o próximo passo consiste em guardar novamente no repositório remoto a nova versão do projecto. Voltamos a fazer os seguintes passos:


- **13.1** Colocar ficheiro na Staging Area  e realizar commit: **git commit -a -m "Fix bug and test email validation (resolve #8)"**


- **13.2** Mudar para a master branch: **git switch main**


- **13.3** Fazer push para remoto: **git push origin fix-invalid-email**



**14** Resolvido o bug e testada a nova implementação, pretende-se por fim integrar todo este trabalho na nossa versão estável da aplicação, isto é, na master branch. Para isso, fazemos um **Fast Forwrd merge**, uma forma de merge que ocorre quando o historial de commits é linear, ou seja, quando se cria uma nova branch e a partir desse momento só existem commits nessa branch. Desta maneira, o Git, em vez de integrar (fazer merge) efectivamente de duas versões, simplifica o processo e move o apontador da master branch para o último commit da branch alternativa. Daí esta ideia de fast-forwarding. Seguimos então os próximos passos:


- **14.1** Fazer o merge: **git merge fix-invalid-email**
 

- **14.2** Push para o Bitbucket: **git push origin**


**15** De seguida, colocamos uma nova tag com a versão v1.3.1: **git tag v1.3.1 -m "nova versão 1.3.1"**

**16** Fazemos o push para o servidor com a nova tag: **git push origin v1.3.1**

**17** Actualizamos o ficheiro README com todas as informações do projecto e fazemos o respectivo commit e push para orginin.

**17** Por fim, marcamos o último commit da parte 2 do CA1 com a tag final pedida no enunciado: **git tag -a ca1-part2 -m "tag last version of ca1-part2"**.

**18** Push para o repositório remoto(Bitbucket): **git push origin ca1-part2**.

<br>

______________________

### Outros comandos Git habitualmente usados

<br>

Ainda que não tenham sido utilizados neste trabalho, existem vários comandos Git que são utilizados com muita frequência no contexto de trabalho. Apresenta-se alguns deles:


**git init** - Permite criar um repositório vazio ou reinicializar um já existente. Cria subdirectório **.git/**, local onde são guardados os ficheiros (esqueleto do repositório);

**git clone** - Permite clonar um repositório já existente para um directório recentemente criado;

**git fetch** - Permite consultar alterações no repositório remoto, tendo em conta o último "pull" no repositório local;

**git pull** - Importa para o repositório local uma cópia da última versão armazenada no repositório remoto.

**git branch -v** - Permite consultar o último commit em cada branch

**git branch --merged** - Apresenta as branches já alvo de merge

**git branch --no-merged** - Apresenta, por outro lado, as branches que ainda não foram merged

_______________________

## CA1 - Resolução Alternativa com Mercurial

**LINK PARA O REPO** : https://sourceforge.net/p/ca1-with-mercurial/code/ci/default/tree/

<br>

Vamos agora implementar as tarefas do CA1, desta vez recorrendo a um sistema de controlo de versões diferente, o Mercurial.

Mas antes de colocarmos mãos à obra, é importante conhecermos um pouco deste VCS.

### O que é o Mercurial?

À semelhança do Git, o **Mercurial** é também uma ferramenta de gestão e controlo de versões distribuído. É igualmente gratuito, open source e foi também lançado em 2005. Apesar de ser utilizado em alguns gigantes tecnológicos, como o Facebook ou Mozilla, representa apenas 2% de utilização no mercado tech.

### Mercurial : Arquitectura e design - como funciona o Mercurial?

Mercurial e Git são dois VCS muito idênticos, ainda assim existem algumas diferenças que vale a pena assinalar:

- **História** - De um modo geral, o Mercurial incentiva a uma cultura de imutabilidade da "história". Enquanto que no Git existem vários comandos que nos permitem modificar commits antigos, no caso do Mercurial isso não é permitido com tanta facilidade. Esta é uma característica que pode garantir mais segurança a utilizadores com pouca experiência;


- **Estrutura do repositório** - O Mercurial não permite os designados "octopus merges", isto é, não é possível fazer merge de mais do que duas branches;


- **Branches** - Enquanto que no Git as branches são apenas referências de commits realizados, no Mercurial os branches são incorporados directamente nos commits. Ora, isto faz com que não seja possível remover branches, pois isso alteraria a história do projecto;


- **Workflow** - No Mercurial não existe uma Staging Area. As alterações ao projecto são directamente armazenadas no repositório;


Existem muitas semelhanças nos comandos de Git e Mercurial. Vejamos já de seguida uma lista com os comandos mais utilizados de um e outro VCS:


| Mercurial   | Git	          |	Descrição	                                            |
|:-----------:|:-------------:|:-------------------:|
| hg add      | git add       | 	No Mercurial, apenas para adicionar novos ficheiros ao repositório. Não existe staging area|
| hg commit   | git commit    |	Para realizar commit	|
| hg push     | git push      |	Para fazer push para repositório remoto	|
| hg tag      | git tag | Para atribuir uma tag |
|hg branch | git branch | Mercurial: conhecer a branch actual. No Git : lista as branches |
|hg branches | ------- | Permite no Mercurial conhecer as branches activas
| hg update | hg switch/checkout | Para mudar de branch |
|hg merge | git merge | Para fazer merge |



### Primeiros passos no Mercurial

**1** O primeiro passo a tomar para desenvolvermos o nosso trabalho é encontrar um serviço que nos permita guardar remotamente as várias versões do nosso projecto (um repositório remoto) e que seja compatível com o Mercurial. Visto que isso já não é possível no Bitbucket, optou-se pelo Source Forge, uma das primeiras plataformas open source a oferecer a possibilidade de armazenamento e controlo de versões.

**2** Criado um repositório no Source Forge, clonamos localmente o repositório  no nosso computador através do comando **hg clone ssh://hugoamorgadinho@hg.code.sf.net/p/ca1-with-mercurial/code ca1-with-mercurial-code**

**3** Para testar a ligação com o remote, realizamos um primeiro commit de teste:

- **3.1** Criar um ficheiro teste.txt : **touch teste.txt**


- **3.2** Adicionar um ficheiro novo ao repositório : **hg add**


- **3.3**Commit para o repositório remoto com as alterações no nosso novo ficheiro: **hg commit -m "My first commit with Mercurial"**


- **3.4** Push para o repositório : **hg push**

<br>


### Parte 1 com Mercurial


**1** Criado o directório CA1_with_Mercurial, copiamos para lá o código do nosso projecto;

**2** Fazemos o primeiro commit neste trabalho com o nosso projecto já no repositório local. A ideia é armazená-lo também no Source Forge:
- Adicionar o ficheiro novo ao repositório: **hg add**


- Commit para o repositório remoto: **hg commit -m "Copy the Tutorial React.js and Spring Data REST Application into CA1_with_Mercurial"**


- Push para o Source Forge: **hg push**


**3** De seguida, colocamos a tag no commit anterior, assinalando a versão v1.1.0 do projecto
- **hg tag v1.1.0**
- **hg push**

**4** À semelhança do Git, podemos também consultar o histórico dos nossos commits. É isso que fazemos através do comando: **hg log**

**5** Implementamos a nova funcionalidade na aplicação e realizamos os respectivos testes. Neste caso, criámos um campo para o número de anos do funcionário;

**6** Fazemos commit para o nosso repositório repositório remoto:
-  **hg commit -m "Implementing new features: job years. Tests done"**
- **hg push**

**7** Depois colocamos a tag com a nova versão v1.2.0:
- **hg tag v1.2.0**
- **hg push**

**8** Consultamos as tags que já utilizámos através do comando: **hg tags**

**9** Actualizamos o markdown com informações relativas ao nosso trabalho.

**10** Por fim, realizamos o último commit relativo à parte 1 do CA1, colocando a tag ca1-part1:
- **hg commit -m "update project - part 1"**
- **hg push**
- **hg tag ca1-part1**
- **hg push**



### Parte 2 com Mercurial


Vamos agora adicionar uma nova funcionalidade, desta vez um campo para e-mail do funcionário

**1** Criamos uma nova branch para esta implementação, deixando a branch principal com a versão estável:
- **hg branch email-field**

Depois de criada a branch, a nossa localização é automaticamente essa nova branch

**2** Implementamos a nova funcionalidade na aplicação e fazer respectivos testes

**3** Aproveitamos agora para consultar o estado da nossa working area através do comando **hg stauts**

Aqui podemos verificar mais uma diferença em relação ao git. As alterações ou tracking dos ficheiros é apresentada de uma maneira bem distinta. Antes de cada ficheiro, é visível um caractere, cada um com um significado distinto:

	**M** = modified
	**A** = added
     	**R** = removed
  	**C** = clean
   	**!** = missing (appa by non-hg command, but still tracked)
   	**?** = not tracked
 	**I** = ignored


**4** Após a referida implementação e testes, fazemos commit:
- **hg commit -m "Implement and test new e-mail feature"**

**5** De seguida, voltamos para a default branch e realizamos o push:
- **hg update default**
- **hg push**

Podemos confirmar a branch em que nos localizamos através do comando **hg branch**

Podemos também consultar as branches existentes através do comando **hg branches**. Este comando apenas mostra a nova branch na lista depois de ter sido feito um commit nessa branch.

**6** Vamos agora integrar o trabalho desenvolvido na branch email-field para a branch default (estável):
- **hg merge**
- **hg commit -m "merge new feature to main app"**
- **hg push**

**7** Finalizado o merge, colocamos a tag com a versão 1.3.0:
- **hg tag v1.3.0**
- **hg push**

**8** Podemos agora eliminar a branch email-field, visto já termos integrado a nova feature no projecto. Depois de alguma pesquisa, identificamos aqui mais uma diferença em relação ao Git: no Mercurial não é possível eliminar branches local e remotamente.

**9** De seguida criamos uma nova branch para resolução de bug, agora relacionado com validação de e-mail. Esta nova branch será intitulada "fix-invalid-email":
- **hg branch fix-invalid-email**

**10** Como anteriormente, somos automaticamente movidos para esta nova branch. Implementamos então a nova validação no projecto, aproveitando para editar alguns atributos na classe Employee, provocando assim intencionalmente um conflito no momento do merge.

**11** Fazemos o commit depois da implementação e testes à nova validação:
**hg commit -m "Fix bug and test new email validation"**

**12** Trocamos para a default branch:
- **hg update default**

**13** Na default brach, executamos agora o merge que como prevemos provocará um conflito:
- **hg merge fix-invalid-email**

**14** Como previmos, o conflito é gerado e as alterações feitas na branch alternativa, e que se sobrepõem à versão original, não são integrados. No IDE, na coluna lateral ao código as diferenças são assinaladas a azul, nas respectivas linhas. Clicando nessa zona azul podemos consultar as diferenças entre as duas versões e optar por uma. É exactamente isso que fazemos para resolver o conflito.

**15** Ultrapassada essa situação, fazemos commit e push para o repositório no Source Forge, assinalando esta nova versão com a tag 1.3.1:
- **hg commit -m "Merge new email validation resolving conflict"**g
- **hg tag v1.3.1**
- **hg push**

**16** De seguida, actualizamos o ficheiro README com todos os passos executados neste projecto

**17** Por fim, assinalamos o último commit com a tag da última versão do trabalho e fazemos push para o repositório remoto:
- **hg commit -m "update readme file"**
- **hg tag ca1-part2 -m "tag last version of the project"**
- **hg push**



### Considerações finais

De um modo geral, não senti diferenças significativas na implementação deste trabalho usando Git ou Mercurial. A semelhança dos comandos, o facto de serem ambos VCS distribuídos, contribuíram certamente para uma experiência relativamente idêntica. A mudança de VCS não foi por isso difícil. O Mercurial é aliás relativamente mais simples e isso é perceptível na prática. Com menos comandos ou opções, se por um lado pode ser encarado como uma vantagem para quem se inicia na utilização de VCS, por outro poderá ser uma limitação importante para utilizadores mais experientes.

Pessoalmente, prefiro a arquitectura do Git relativamente ao fluxo de trabalho. Vejo vantagens importantes na existência da Staging Area, por exemplo, que permite organizar de melhor maneira o que pretendemos armazenar em determinado momento no repositório, ou o seu branching mais flexível.

Como já referido, são sistemas muito idênticos, mas julgo que o Git poderá ser uma ferramenta bem mais poderosa e versátil a longo prazo e para projectos mais ambiciosos.

A maior dificuldade talvez tenha sido a utilização do Source Forge. Ao contrário do Bitbucket, o Source Forge não oferece uma UI que nos permita visualizar com maior clareza o rumo do nosso repositório, dando-nos apenas a listagem de commits. Só recorrendo ao Tortoise Workbench pude observar a nível local o fluxo de trabalho. Neste aspecto, é visível uma clara desvantagem relativamente ao Bitbucket.

A nível de documentação e apoio também não senti muitas diferenças. Dada a sua popularidade, é obviamente mais fácil encontrar apoio para Git, ainda assim o Mercurial não fica nada atrás, sendo a documentação "oficial" de leitura muito fácil e esclarecedora. Para quem se estreia no uso de VCS pelo Git não terá grande dificuldade em adaptar-se ao Mercurial. São ambos softwares com um grau de aprendizagem relativamente acessível e muito úteis na gestão diária do nosso trabalho.

