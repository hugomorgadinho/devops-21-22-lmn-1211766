# Class Assignment 5

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

# Parte II Alternativa - *Continuous Integration and Delivery* com *Buddy*

<br>

## O que é o Buddy e como pode ser utilizado

<br>

***Buddy*** é uma das várias soluções disponíveis actualmente para tarefas CI/CD. Ao contrário do Jenkins, que se caracteriza por ser *server-based* (isto é, funciona a partir de um *container serverlet*), o *Buddy* é *web-based* e *self-hosted*. Quer isto dizer que não é necessária qualquer instalação prévia, basta apenas criar conta no serviço para se poder desempenhar funções de *build*, *test* ou *deploy*. O serviço apresenta-se como sendo uma solução *beginner friendly* e *user friendly*, apostando numa customização de projecto e das respectivas práticas de DevOps muito orientada a GUI, ainda que permita também a construção de *pipelines as code* (através de um ficheiro Yaml).

O Buddy foi criado para *developers* que utilizem o Git como sistema de controlo de versões e é compatível com aplicações armazenadas no GitHub, Bitbucket ou GitLab.

Relativamente ao seu funcionamento, o *Buddy* gera um Docker container para realizar as suas acções.

Apresenta-se de seguida alguns dos conceitos-chave do serviço:

- ***Pipeline***: é considerado o conceito central da plataforma. Tal como o Jenkins, é na pipeline que se descrevem as acções como *build*, *test* ou *deploy* de uma aplicação. As *pipelines* podem ser espoletadas manualmente, em cada *push* para o repositório ou ainda em períodos agendados;


- ***Action***: Cada etapa de uma pipeline é caracterizada por *action*. Uma *action* será executada num Docker container, sendo que cada uma dessas acções consiste num conjunto de comandos na *shell*. As acções podem ser, por exemplo, fazer build, executar testes unitários ou fazer upload no servidor, entre outras. É possível configurar as variáveis de ambiente de cada *action*;


- ***Execution***: Uma execução é um simples *run* da *pipeline*. Cada execução é guardada e é possível consultar os resultados dessa run;


- ***Integrations***: São serviços *third-party* que podem ser integrados nas *pipelines* do *Buddy*, como repositórios, website hosting services, testing tools e notification apps. Alguns exemplos destes serviços externos são: GitHub, AWS, Heroku, Slack, GitLab, Cloudflare, Firebase, entre outros;


Existem algumas diferenças entre *Jenkins* e *Buddy*. Destacam-se algumas:

<br>



| Categoria  | Jenkins | Buddy    |
|:-----------:|:-------------: |:-------------: |
| ***Instalação*** | Requer instalação de aplicação Java | Não é necessário instalação   |
| ***Manutenção*** | Requer alguma manutenção ou actualização periodicamente | Não necessita de qualquer tipo de manutenção|
| ***Integração*** | Através de *plugins open-source* | Já disponíveis no serviço |
| ***Pipelines*** | *Script* e *UI driven* | *Script* e *UI driven* |
| ***Customização*** | Alto nível de controlo das integrações | Muitas possibilidades de integração mas baixo controlo na customização de serviços externos |

<br>

## Relatório técnico

<br>

Pretende-se agora replicar o **CA5 Part 2**, utilizando desta vez o *Buddy* como ferramenta de CI/CD para construção da *pipeline*.

### 1. Criar conta no *Buddy*

Para criar conta no *Buddy* e utilizar a plataforma basta fazer o registo através do link: https://buddy.works . O serviço é grátis para um plano de até 5 projectos.

<br>

### 2. Criar uma nova *pipeline*


Com a conta criada e o *login* feito, é agora possível criar uma *pipeline*:

**2.1** Em Dashboard clicar em `Create a new project`;



**2.2** Seleccionar o repositório remoto, no nosso caso escolhemos o repositório Bitbucket individual de Devops;



**2.3** Com a integração ao repositório Bitbucket, clica-se em `Add new pipeline`, e preenche-se os campos da seguinte maneira:


- `Name`: CA5-Part2-Alternative


- `Purpose` : CI/CD: Build, test, deploy


- `Trigger` : Manually


- `Branch` : Main


- Por fim, clicar em `Add pipeline`


No Buddy é possível criar a *pipeline* através do GUI e depois exportar a pipeline criada para um ficheiro YAML. É isso que faremos, colocando por fim o ficheiro Yaml no repositório remoto para posterior construção da pipeline.

<br>

### 3. Adicionar as *actions* à pipeline



Feitas as configurações gerais da *pipeline*, é agora possível descrever as etapas da *pipeline*. Na descrição das etapas, ou no caso do Buddy, das *actions*, esse processo é bem diferente do Jenkins. Veremos de seguida como se faz com o *Buddy*

**3.1** No caso dos comandos shell de gradle, define-se o serviço **Gradle**, como é visível na imagem seguinte. Depois de definido esse serviço, é possível definir os comandos shell:

<br>

![ca5_actions](imagens/add-action-buddy.PNG)


<br>


**3.2.** No caso das *actions* relativas ao build da imagem Docker e de respectivo push para Docker Hub, selecciona-se o serviço Docker. A Dockerfile utilizada é a mesma utilizada no **CA5 Part 2**. No caso do *push*, é necessário definir na *action* uma Docker hub account (username e password). A conta fica automaticamente associada. A configuração do push fica com o seguinte aspecto:

<br>

![ca5_docker_hub](imagens/buddy-push-docker-image.PNG)

No final, ficamos com uma *pipeline* com as seguintes *actions*:

<br>

![ca5_buddy-actions](imagens/buddy-actions.PNG)

<br>

### 4. Fazer *run* da *pipeline* e criar ficheiro Yaml

**4.1** Para verificar que a pipeline executa o build com sucesso, fazemos run directamente na GUI do *Buddy*. Depois de algumas falhas, executa-se a build, clicando em `Run`, e obtemos finalmente a mensagem de sucesso da operação:

<br>

![ca5_buddy-pipeline-run](imagens/buddy-pipeline-passed.PNG)

<br>

Podemos verificar que a imagem foi também guardada no Docker Hub:

<br>

![ca5_buddy-actions](imagens/docker-hub-ca5-part2-alternative.PNG)

<br>

**4.2** É agora possível através do Buddy gerar um ficheiro Yaml. Para isso, executa-se os seguintes passos:

- Nas definições da pipeline, clica-se em `Download YAML config`;

O ficheiro YAML ficará com o seguinte aspecto:


```
- pipeline: "CA5-Part2-Alternative"
 on: "CLICK"
 refs:
 - "refs/heads/main"
 priority: "NORMAL"
 fail_on_prepare_env_warning: true
 actions:
 - action: "Assemble"
   type: "BUILD"
   working_directory: "/buddy/devops-21-22-lmn-1211768"
   docker_image_name: "library/gradle"
   docker_image_tag: "latest"
   execute_commands:
   - "echo Compiling...''"
   - "cd CA5/Part_2_Alternative/gradle_basic_demo"
   - "chmod +x gradlew"
   - "./gradlew tasks clean assemble"
   volume_mappings:
   - "/:/buddy/devops-21-22-lmn-1211768"
   cache_base_image: true
   shell: "BASH"
 - action: "Test"
   type: "BUILD"
   working_directory: "/buddy/devops-21-22-lmn-1211768"
   docker_image_name: "library/gradle"
   docker_image_tag: "latest"
   execute_commands:
   - "echo 'Running tests...'"
   - "cd CA5/Part_2_Alternative/gradle_basic_demo"
   - "./gradlew test"
   volume_mappings:
   - "/:/buddy/devops-21-22-lmn-1211768"
   cache_base_image: true
   shell: "BASH"
 - action: "Generate Jar"
   type: "BUILD"
   working_directory: "/buddy/devops-21-22-lmn-1211768"
   docker_image_name: "library/gradle"
   docker_image_tag: "latest"
   execute_commands:
   - "echo 'Generating jar...'"
   - "cd CA5/Part_2_Alternative/gradle_basic_demo"
   - "./gradlew jar"
   volume_mappings:
   - "/:/buddy/devops-21-22-lmn-1211768"
   cache_base_image: true
   shell: "BASH"
 - action: "Javadoc"
   type: "BUILD"
   working_directory: "/buddy/devops-21-22-lmn-1211768"
   docker_image_name: "library/gradle"
   docker_image_tag: "latest"
   execute_commands:
   - "echo 'Creating Javadoc...'"
   - "cd CA5/Part_2/gradle_basic_demo"
   - "chmod +x gradlew"
   - "./gradlew javadoc"
   volume_mappings:
   - "/:/buddy/devops-21-22-lmn-1211768"
   cache_base_image: true
   shell: "BASH"
 - action: "Archive"
   type: "ZIP"
   local_path: "CA5/Part_2/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar"
   destination: "CA5/Part_2/gradle_basic_demo/build/distributions/*.zip"
   deployment_excludes:
   - ".git"
 - action: "Build Docker image"
   type: "DOCKERFILE"
   dockerfile_path: "CA5/Part_2_Alternative/gradle_basic_demo/Dockerfile"
   target_platform: "linux/amd64"
 - action: "Push Docker image"
   type: "DOCKER_PUSH"
   docker_image_tag: "ca5-part-2-alternative"
   repository: "hugoamp/ca5-part2-alternative"
   integration_hash: "Pr1gO3GWpmbkBlxEazJn4KRjLM"

```

<br>


- Nas definições do projecto, clica-se em `Switch to YAML`;


- Colocar o ficheiro YAML na raiz do repositório;


Desta maneira, de cada vez que se executar `Run`, a pipeline será construída a partir do ficheiro `buddy.yml`


*Nota: Não foi possível implementar a etapa de jUnit para publicação de resultados, bem como a publicação dos reports HTML, visto não ter sido possível encontrar esses serviços de integração no Buddy*


## Conclusão

As diferenças entre Buddy e Jenkins são agora mais notórias depois da realização deste trabalho. Se por um lado o Buddy é claramente mais intuitivo na construção de pipelines e, tal como o serviço se apresenta, *user friendly* para quem ainda não domina as práticas DevOps, por outro lado nota-se que a customização não é tão livre como no Jenkins, já que os serviços de integração externos estão em grande medida pré-configurados. Relativamente ao Jenkis, o Buddy também é bastante mais rápido na excução da pipeline. Por falta de documentação do serviço e ainda alguma inexperiência, não foi possível construir com sucesso um ficheiro yaml antes da construção da pipeline na GUI. Por outro lado, com Jenkins existe muito mais documentação de apoio para a construção da Jenkinsfile.

Ainda assim, este trabalho permitiu perceber as potencialidades destas ferramentas CI/CD no processo de construção de software, nomeadamente no que diz respeito à ajuda que podem fornecer na construção de aplicações de forma rápida e com código mais seguro e de melhor qualidade.
