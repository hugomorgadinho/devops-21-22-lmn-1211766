# Class Assignment 5

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte II - *Continuous Integration and Delivery* com Jenkins

<br>


### 1. Relatório técnico

Neste **CA5 Parte II**, o objectivo é criar uma nova *pipeline* utilizando o **Jenkins**, desta vez com os seguintes *stages*:

- **Checkout**: Verificar o código do repositório;


- **Assemble**: Compilar o código da aplicação e produzir os artefactos com os respectivos resultados;


- **Test**: Executar os testes unitários da aplicação. Será criada um processo *post* do jUnit para publicar os resultados dos testes feitos;


- **Javadoc**: Gerar os *javadoc* do projecto e publicá-los no Jenkins. Pretende-se ainda publicar as informações geradas em *reports* html;


- **Archive**: Arquivar no Jenkins os ficheiros *archive* gerados durante a etapa *Assemble*;


- **Publish Docker Image**: Criar uma *docker image* a executar no servidor Tomcat, gerando posteriormente o ficheiro war., e publicando de seguida a imagem na conta pessoal do Docker Hub.


<br>


#### 1.1. Instalar novos *plugins* no Jenkins

Antes de começar a criar a nova *pipeline*, cria-se uma nova pasta para este **CA5 Part_2**, para onde será copiada a aplicação *App Chat*.

**1.1.1** Para começar o trabalho propriamente dito, faz-se *login* para iniciar o Jenkins:

- Na pasta onde foi guardado o ficheiro *war* do Jenkins, executa-se o comando: `java -jar jenkins.war`;


- Com a aplicação em modo *running*, abrimos no browser o endereço `http://localhost:8080`;


- Faz-se então o *login* com as credenciais pessoais (username e password).



**1.1.2** É necessário instalar dois *plugins* para executar os dois novos *stages*, publicar os *reports* html gerados a partir dos *javadoc* e ainda utilizar o *Docker* na execução da *pipeline* . Os plugins necessários são: **HTML Publisher** e **Docker Pipeline**.


Para instalar estes plugins, executa-se os seguintes passos:


*Manage Jenkins* >> *Manage plugins* >> seleccionar *Available*.


De seguida, selecciona-se os plugins anteriormente referidos e clica-se em instalar.


**1.1.3** Reinicia-se o Jenkins.

<br>


#### 1.2. Criar uma nova *pipeline* no Jenkins


Pretende-se agora criar uma nova *pipeline*, definindo as várias fases de construção da aplicação. Tal como na **Part I**, a ideia é executar a *pipeline* a partir de um **Jenkinsfile**.

**1.2.1.** Antes de mais, é necessário criar a *pipeline* no Jenkins (ou um novo *Job*):

- Clicar em `New Item` e atribuir o nome **CA5-Part2** ;


- Seleccionar `Pipeline`.


**1.2.2.** Para criar a *pipeline* a partir de um **Jenkinsfile**, é necessário proceder aos seguintes passos:

- Em `Definition`, escolher: `Pipeline script from SCM`;


- Definir o Source Control Manager: `Git`;


- Colocar o url do repositório: `https://bitbucket.org/hugomorgadinho/devops-21-22-lmn-1211766/src/main/`;


- Especificar a branch: `*/main`;


- Definir o script path: `CA5/Part_2/gradle_basic_demo/Jenkinsfile`;

<br>

#### 1.3. Criar e configurar *Jenkinsfile*

**1.3.1.** Agora podemos criar o **Jenkinsfile**. Na pasta da aplicação *gradle basic demo*, cria-se um ficheiro (sem extensão): `touch Jenkinsfile`.

**1.3.2.** Para executar as etapas já descritas, descreve-se a seguinte Jenkinsfile:

<br>


```
pipeline {
   agent any

   stages {
       stage('Checkout') {
           steps {
               echo 'Checking out...'
               git branch: 'main', url: 'https://hugomorgadinho@bitbucket.org/hugomorgadinho/devops-21-22-lmn-1211766'
           }
       }

       stage('Assemble') {
         steps {
           echo 'Compiling...'
           dir('CA5/Part_2/gradle_basic_demo'){
           bat './gradlew tasks clean assemble'
           }
         }
       }

       stage('Test') {
           steps {
               echo 'Running tests...'
               dir('CA5/Part_2/gradle_basic_demo'){
               bat './gradlew test'
               }
           }
       }


          stage('Generate Jar') {
                   steps {
                       echo 'Generating jar...'
                       dir('CA5/Part_2/gradle_basic_demo') {
                       bat './gradlew jar'
                       }
                   }
          }

               stage('Javadoc') {
                   steps {
                       echo 'Creating Javadoc...'
                       dir('CA5/Part_2/gradle_basic_demo') {
                           bat './gradlew javadoc'
                       }
                   }
               }

       stage('Archive') {
           steps {
               echo 'Archiving...'
                dir('CA5/Part_2/gradle_basic_demo'){
               archiveArtifacts 'build/distributions/*'
               }
           }
       }

        stage ('Docker Image') {
                   steps {
                       echo 'Generating Docker image...'
                       dir('CA5/Part_2/gradle_basic_demo') {
                           script {
                               docker.withRegistry('https://registry.hub.docker.com', 'dockerhub_credentials') {
                                   def customImage = docker.build("hugoamp/ca5_part_2:${env.BUILD_ID}")
                                   customImage.push()
                               }
                           }
                       }
                   }

       }
   }

   post {
       always {
           junit 'CA5/Part_2/gradle_basic_demo/build/test-results/test/*.xml'
           publishHTML([allowMissing: false,
                           alwaysLinkToLastBuild: false,
                           keepAll: false, reportDir: 'CA5/Part_2/gradle_basic_demo/build/docs/javadoc',
                           reportFiles: 'index.html',
                           reportName: 'HTML Report',
                           reportTitles: 'The Report'])
       }
   }
}
```

<br>


#### 1.4. Adicionar credenciais *Docker Hub*

**1.4.1** De maneira a ser possível fazer o push da *Docker image* na conta pessoal do **Docker Hub**, durante a execução da *pipeline*, é necessário atribuir os dados de *login* no Jenkins. Para isso, executa-se os seguintes passos:


*Manage Jenkins* >> *Manage Credentials* >> *Jenkins* >> *Global credentials (unrestricted)* >> *Add credentials*:

**1.4.2** A seguir, preenche-se os seguintes campos:

- **Kind**: *Username with password*;


- **Scope**: Global;


- **Username**: *username* do Docker Hub;


- **Password**: *password* do Docker Hub;


- **ID**: definiu-se *dockerhub_credentials*


Por fim, selecciona-se o botão **OK**.


<br>


#### 1.5. Definir *Dockerfile* para criar *Docker image*


**1.5.1.** Pretende-se agora escrever o *Dockerfile* que gere uma imagem e respectivo *container* durante execução da *pipeline*. Na pasta da aplicação, cria-se então o seguinte *Dockerfile*:


```
# Download base image ubuntu 18.04
FROM ubuntu:18.04

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Install apt-get
RUN apt-get update -y

#Install java-jdk11
RUN apt-get install openjdk-11-jdk-headless -y

# Copy the jar file into the container
COPY build/libs/basic_demo-0.1.0.jar .

# Expose Port
EXPOSE 59001

# Execute server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

Tal como definido no **Jenkinsfile**, durante a execução da stage `Docker Image`, será feito o push para o **Docker Hub**;


<br>


#### 1.6. Executar a build e ver os resultados

**1.6.1.** Antes de fazer o build no Jenkins, é necessário fazer o commit e push para o repositório.

**1.6.2.** Podemos agora executar o build, clicando em `Build Now` e acompanhar todas as fases da pipeline. Depois de algumas builds falhadas, conseguimos por fim executar a build com sucesso:

![ca5_builds](imagens/ca5-part2-build-pipeline.png)

**3.3.** Confirmamos também no Docker Hub que a imagem foi publicada:

![ca5_tests](imagens/ca5-part2-docker-hub.png)

