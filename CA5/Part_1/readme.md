# Class Assignment 5

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte I - *Continuous Integration and Delivery* com Jenkins

<br>

### 1. O que é *Continuous Integration*?

*Continuous Integration* (**CI**), ou em português - Integração Contínua -, é um conjunto de práticas que têm como objectivo integrar de forma automatizada as alterações do código, isto é, a evolução do projecto, feitas por vários "contribuidores" num repositório partilhado. É um dos princípios basilares de DevOps e permite que os *developers* trabalhem sobre a versão mais recente do projecto.

Esta integração é feita recorrendo a um sistema de controlo de versões, como Git. É aqui que entram em cena as ferramentas de integração contínua como Jenkins. Em cada nova versão, o código será compilado e analisado, ajudando a equipa de desenvolvimento a assegurar que o código poderá ser colocado em produção

<br>

### 2. O que é o Jenkins e por que é utilizado?

**Jenkins** é uma ferramenta que permite implementar os princípios CI/CD (continuous integration / continuous delivery) durante o desenvolvimento de software. Ajuda a automatizar um conjunto de tarefas, como compilação, testes e *deploying*. É um sistema *server-based*, pois corre num *container serverlet*, como o Apache Tomcat. Tem suporte para vários sistemas de controlo de versões, como AccuRev, CVS, Subversion, Git, Mercurial, Perforce, ClearCase ou RTC, e pode executar várias *building tools* como Ant, Maven ou mesmo Gradle.

Jenkins veio tornar mais rápido o processo de desenvolvimento de software, já que permite integrar de forma muito simples as várias alterações a um projecto. A automatização de todas estas tarefas (*building, testing, deploying*) permite aumentar a produtividade das equipas de desenvolvimento.

Com Jenkins é possível realizar inúmeras tarefas, como agendar *buildings*, obter um relatório com os resultados dos testes, transferir builds para a respectiva aplicação em funcionamento/web server, conhecer as métricas de todas as builds, entre outras.

Todas estas funcionalidades podem ser automatizadas através de uma **pipeline**, que de um modo geral é o conjunto de “eventos” definidos durante o ciclo de construção do código. As pipelines são habitualmente escritas na linguagem Groovy num ficheiro Jenkinsfile. No relatório técnico abordaremos mais ao detalhe este tipo de ficheiro.

Existem alguns conceitos importantes em Jenkins. Antes de se avançar para o relatório, apresentam-se os mais relevantes:

- ***Job***: Um processo (ou evento), que tanto pode ser uma *pipeline* completa ou uma tarefa particular. Determina todo o processo de *build* no Jenkins;


- ***Node***: Faz parte do ambiente Jenkins e executa *pipelines* ou *jobs*;


- ***Stage***: Determina uma etapa do processo de construção do código, e na qual são descritos todos os passos dessa etapa;


- ***Step***:Uma simples tarefa no processo de construção do código.

<br>




### 3. Relatório técnico

Neste **CA5 Parte I** vamos utilizar o **Jenkins** para construir uma simples *pipeline*, utilizando a aplicação *App Chat* (basic gradle demo) do CA2 Part_1 e presente no meu repositório pessoal. A aplicação está também disponível no repo `https://bitbucket.org/luisnogueira/gradle_basic_demo/)`.

**1. Instalar o Jenkins**


**1.1.** Para realizar o trabalho é necessário antes de mais instalar o Jenkins. Para isso, fazemos o download do ficheiro War em `https://www.jenkins.io/download/`


**1.2.** Feito o download da versão estável 2.332.3 (package genérico), executamos no terminal o ficheiro através do comando `java -jar jenkins.war`. Durante a execução será gerada uma password que será necessária durante as configurações de instalação.


**1.3.** A seguir abrimos no browser `http://localhost:8080` (porta que será ocupada pelo servidor) para prosseguir com as configurações de instalação.


**2. Criar uma *pipeline* para a aplicação**

O objectivo agora é criar uma pipeline, definindo as várias fases de construção da aplicação. A ideia é não correr um *script* directamente no Jenkins, mas sim num ficheiro guardado no repositório. Para isso é necessário criar um **Jenkinsfile**.

**2.1.** Para descrever a *pipeline* (fases de *build* do software), começamos por criar um novo *Job* no Dashboard do Jenkins:

- Primeiro clicando em `New Item` com o nome **CA5-Part1** ;


- De seguida seleccionando `Pipeline`.


**2.2.** Para criar a *pipeline* a partir de um **Jenkinsfile**, nas configurações é necessário proceder aos seguintes passos:

- Em `Definition`, escolher: `Pipeline script from SCM`;


- Definir o Source Control Manager: Git;


- Colocar o url do repositório: `https://bitbucket.org/hugomorgadinho/devops-21-22-lmn-1211766/src/main/`;


- Especificar a branch: `*/main`;


- Definir o script path: `CA2/Part_1/gradle_basic_demo/Jenkinsfile`;


**2.3.** A seguir pretende-se então descrever um ficheiro **Jenkinsfile** na pasta da aplicação gradle basic demo. Para isso, na pasta da aplicação, basta criar um ficheiro (sem extensão de ficheiro): `touch Jenkinsfile`.

**2.2.** Definimos então a pipeline de uma forma declarativa, com as etapas:

- **Checkout**: Verificar o código do repositório;


- **Assemble**: Compilar o código da aplicação e produzir os artefactos com os respectivos resultados;


- **Test**:Executar os testes unitários da aplicação. Será criada um processo *post* do jUnit para publicar os resultados dos testes feitos;


- **Archive**: Arquivar no Jenkins os ficheiros *archive* gerados durante a etapa *Assemble*.

Para executar estas etapas descritas na pipeline, será necessária a seguinte Jenkinsfile:

```
pipeline {
   agent any

   stages {
       stage('Checkout') {
           steps {
               echo 'Checking out...'
               git branch: 'main', url: 'https://hugomorgadinho@bitbucket.org/hugomorgadinho/devops-21-22-lmn-1211766'
           }
       }
       stage('Assemble') {
         steps {
           echo 'Building...'
           dir('CA2/Part_1/gradle_basic_demo'){
           bat './gradlew tasks clean assemble'
           }
         }

       }
       stage('Test') {
           steps {
               echo 'Running tests...'
               dir('CA2/Part_1/gradle_basic_demo'){
               bat './gradlew test'
               }
           }
       }
       stage('Archive') {
           steps {
               echo 'Archiving...'
                dir('CA2/Part_1/gradle_basic_demo'){
               archiveArtifacts 'build/distributions/*'
               }
           }
       }
   }

   post {
       always {
           junit 'CA2/Part_1/gradle_basic_demo/build/test-results/test/*.xml'
       }
   }
}
```
<br>

**3. Executar a build e ver os resultados**

**3.1.** Criado o *Jenkinsfile*, é necessário fazer o commit e push para o repositório. De recordar que o ficheiro deverá estar na mesma pasta da aplicação.

**3.2.** Podemos agora executar o build, clicando em `Build Now` e acompanhar todas as fases da pipeline. Depois de algumas builds falhadas, conseguimos por fim executar a build com sucesso:

![ca5_builds](imagens/builds.png)

**3.3.** Consultamos também os resultados apresentados dos testes unitários:

![ca5_tests](imagens/tests.png)
