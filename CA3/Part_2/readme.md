# Class Assignment 3

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte II - Introdução ao Vagrant

<br>

### O que é e para que serve o Vagrant?

Tal como vimos na anterior *Part I*, a virtualização fornece vantagens muito interessantes. Podemos destacar, por exemplo, a possibilidade de reproduzir ambientes de desenvolvimento isolados ou a construção de várias máquinas (até com sistemas operativos diferentes) num único host.

Se recriar uma máquina virtual (VM) é sempre um processo complexo, lidar com a configuração manual de várias VM’s pode ser uma tarefa bastante árdua.

Para facilitar todo este processo, o *Vagrant* permite recriar de forma automatizada um ou mais ambientes virtuais, isto é, VM’s. Para isso, basta escrever o setup das VM’s pretendidas num ficheiro de configuração. Este ficheiro é denominado por *Vagrant file* e nele são descritos todos os passos necessários para a criação do referido ambiente virtual.

Ficamos assim com uma “caixa” para desenvolvimento. Uma *box*, que no Vagrant representa precisamente um formato de ambiente de desenvolvimento. Assim, numa ou mais máquinas, podemos replicar um ambiente com características já predefinidas.

O Vagrant permite executar vários sistemas operativos (Linux, Windows ou macOS) e é compatível com diferentes hipervisors, como por exemplo VirtualBox, KVM ou VMWare.

## Relatório técnico - Vagrant - criar ambientes virtuais de forma automatizada

Vamos agora passar à parte prática do relatório e utilizar o Vagrant para criar de maneira automatizada VM 's predefinidas.


<br>


### 1. Instalar Vagrant

*1.1.* Seguindo as indicações da aula teórica, começamos por instalar o *Vagrant* através do respectivo site``(https://www.vagrantup.com/downloads.html)`` e escolhemos a versão relativa ao nosso sistema operativo.


<br>


### 2. Utilizar o Vagrant

*2.1* Para confirmar a instalação do Vagrant, podemos utilizar o comando ``vagrant -v``. O output é a versão instalada do Vagrant. No nosso caso a versão ``2.2.19``.

Antes de configurar o Vagrant file que utilizaremos para recriar as VM’s pretendidas para este trabalho, apresentam-se alguns comandos Vagrant e respectivas funções:


| Comando  | Descrição                                                  |
|:-----------:|:-------------:
|  vagrant init envimation/ubuntu-xenial| Cria um novo ambiente virtual (e respectivo vagrant file), neste caso gera box envimation/ubuntu-xenial      |   
| vagrant up | Inicializa uma ou mais VM's |
| vagrant halt | Desliga uma ou mais VM's |
| vagrant ssh | Inicia uma sessão SSH da VM |
| vagrant reload --provision | Inicializa a VM com novas configurações realizadas |
| vagrant status | Apresenta o estado das VM's |


<br>


### 3. Configurar Vagrantfile

**3.1.** Começamos por fazer o git clone do repositório fornecido, onde se encontra o Vagrantfile base que iremos configurar para poder criar um ambiente que permita executar a nossa aplicação `React-and-Spring-data-rest-basic`, desenvolvida no CA2.

**3.2.** Feito o clone, copiamos o referido Vagrantfile para a pasta CA3 Part_2. Este Vagrantfile irá criar 2 VM’s: Uma VM **web** que irá executar a nossa aplicação dentro do servidor Tomcat e outra VM **db** que irá correr separadamente a base de dados H2 em modo servidor. A VM web irá ligar-se a esta VM.

**3.3.** Dado que a nossa aplicação tem como plugin java o *JDK11*, vamos instalar uma box do Ubuntu mais recente, nomeadamente a box **ubuntu/bionic64**. No Vagrantfile, substituímos a box `envimation/ubuntu-xenial` pela `ubuntu/bionic64` nas seguintes linhas:

Box predefinida para o ambiente virtual que pretendemos:

```
Vagrant.configure("2") do |config|
config.vm.box = "ubuntu/bionic64"
 
```

Na VM **db** especificamos a box:
```
config.vm.define "db" do |db|
db.vm.box = "ubuntu/bionic64"
db.vm.hostname = "db"
db.vm.network "private_network", ip: "192.168.56.11"
 
```

Na VM **web** configuramos a mesma box:

```
config.vm.define "web" do |web|
web.vm.box = "ubuntu/bionic64"
web.vm.hostname = "web"
web.vm.network "private_network", ip: "192.168.56.10"
 
```

**3.4.** Para poder utilizar a nossa aplicação em modo mais "profissional", e não em modo de desenvolvimento, vamos fazer o deployment para o servidor web Tomcat. Tudo isto é executado primeiro através do clone do nosso repositório e de seguida definimos o comando para a compilação da aplicação. Por fim, é gerado um ficheiro war que é copiado para o servidor Tomcat. Para que isto seja executado pelo Vagrant, fazemos as alterações:

```
# Clone my personal repository to execute the tutorial react and spring data rest basic, gradle "basic" version
     git clone https://hugomorgadinho@bitbucket.org/hugomorgadinho/devops-21-22-lmn-1211766.git
     cd devops-21-22-lmn-1211766/CA2/Part_2/react-and-spring-data-rest-basic
     chmod u+x gradlew
     ./gradlew clean build
     # To deploy the war file to tomcat8 do the following command:
     sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
```

**3.4.1.** Para garantir que o Vagrantfile é executado sem problemas quando é feito o clone, é necessário tornar o repositório remoto de acesso público.


<br>


### 4. Configurar aplicação Spring

Para então garantir que a nossa aplicação pode utilizar a base de dados no servidor H2, a correr na VM **db**, fazemos as alterações necessárias.

**4.1** Começamos pelo ficheiro `build.gradle`. Fazemos as seguintes alterações:

- Em `plugins`

```
plugins {
  id 'org.springframework.boot' version '2.6.6'
  id 'io.spring.dependency-management' version '1.0.11.RELEASE'
  id 'java'
  id "org.siouan.frontend-jdk11" version "6.0.0"
  id 'war'
}
```

- Em `dependencies`, para garantir o suporte do ficheiro `war` e o deployig para o Tomcat, acrescentamos:

`
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'`


- Em `frontend`, deixamos apenas o node.js e o ``assembleScript = 'run webpack'``

**4.2.** Alteramos o ficheiro `package.json`, definindo em `scripts`: `"webpack": "webpack"`.

**4.3.** Adicionamos a class java `ServletInitializer`

**4.4.** Alteramos o ficheiro `application.properties`, ficando:

```
server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

**4.5.** No script `app.js`, alteramos a função `componentDidMount()`, adicionando o application context path:

```
componentDidMount() { // <2>
     client({method: 'GET', path: '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
        this.setState({employees: response.entity._embedded.employees});
     });
  }
```

**4.6.** Alteramos o `href` do ficheiro css, no ficheiro `index.html`.

**4.7.** Podemos agora executar o comando `vagrant up` para criar e configurar as guest VM's de acordo com o Vagrantfile.

<br>


### 5. Utilizar a aplicação

**5.1.** Terminadas as configurações e criadas as VM's, podemos testar a aplicação web React-Spring através das seguintes opções:

- `http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/`


- `http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/`

Como esperado, podemos visualizar o frontend a correr na VM *web*:

![ca3_web1](images/ca3_web1.png)

**5.2.** Testamos também a consola da base de dados H2, através das seguintes opções:

- `http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console`


- `http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console`

**5.3.** Na consola, colocamos o url para ligar à base de dados: `jdbc:h2:tcp://192.168.56.11:9092/./jpadb`

**5.4.** Inserimos dados na base de dados:

![ca3_db1](images/ca3_db1.png)

**5.5.** Por fim, confirmamos no frontend a introdução dos dados:

![ca3_web1](images/ca3_web2.png)


  