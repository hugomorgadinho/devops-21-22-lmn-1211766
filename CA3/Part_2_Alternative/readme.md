# Class Assignment 3

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Parte II - Alternativa com VMware Workstation Player

<br>

### O que é o VMware?

O VMware é um fornecedor de software de virtualização e cloud computing. É uma das principais empresas de virtualização e disponibiliza várias soluções para desktop ou servidor, hypervisors do tipo 1 e 2.

Para este trabalho, vamos utilizar a versão de desktop gratuita, o **VMware Workstation 16 Player**. Esta versão pode ser instalada apenas no Windows ou Linux. No entanto, suporta uma ampla variedade de sistemas operativos guest.

## Relatório técnico

Passemos então agora para o trabalho prático, beneficiando da automatização do Vagrant, mas criando as VM através do VMware Workstation.

<br>


### 1. Instalar o VMWare e Configurações iniciais

**1.1.** O primeiro passo é então instalar o VMware Workstation Player realizando o download da versão do nosso sistema operativo no site oficial `https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html`.

**1.2.** Feita a instalação do VMware, é necessário instalar também o package **Vagrant VMware Utility** no site do Vagrant: `https://www.vagrantup.com/docs/providers/vmware/vagrant-vmware-utility`.

**1.3.** De seguida, instala-se o plugin para o provider VMware executando o comando: `vagrant plugin install vagrant-vmware-desktop`.

**1.4.** Caso o plugin não seja acima da versão 1.0.0, é necessário fazer um update através do comando `vagrant plugin update vagrant-vmware-desktop`.

Podemos agora utilizar o VMware através do Vagrant.

### 2. Configurar o Vagrantfile

Para este trabalho alternativo, optámos por fazer uma cópia do Vagrantfile utilizado na parte anterior, realizando depois as configurações necessárias para o VMware. Ainda assim, seria também possível começar por gerar um novo Vagrantfile através do comando `vagrant init` e instalando de seguida a box `vagrant box add hashicorp/bionic64`.

No nosso caso, efectuámos as seguintes alterações no Vagrantfile:

**2.1.** Nas configurações iniciais, definir a box a utilizar para as 2 VM e a respectiva versão:

```
 Vagrant.configure("2") do |config|
   config.vm.box = "hashicorp/bionic64"
   config.vm.box_version = "1.0.282"
```

**2.2.** Definir ainda assim em cada VM particular a sua box:

Na VM db:
```
  db.vm.box = "hashicorp/bionic64"
```
E ainda na VM web:
```
 web.vm.box = "hashicorp/bionic64"
```

**2.3.** Alterar o provider para VMware:

```
"vmware_desktop"
```

**2.4.** Redefinir os IP das VM para a nova rede. Para isto foi necessário utilizar a rede privada gerada automaticamente pelo VMWare aquando da sua instalação. Foi assim necessário verificar no gestor de redes do computador o ip desta rede VMnet. No nosso caso, utilizamos a rede com o IPv4 `192.168.174.1`.

Assim sendo, definimos as seguintes redes para as nossas VM no Vagrantfile:

Para a VM db:
```
db.vm.network "private_network", ip: "192.168.174.11"

```

Para a VM web:

```
web.vm.network "private_network", ip: "192.168.174.10"
```


### 3. Configurar application.properties

Terminadas as configurações no Vagrantfile, é também necessário alterar o ficheiro `application.properties` na nossa aplicação, nomeadamente para a criação da base de dados h2:

``
spring.datasource.url=jdbc:h2:tcp://192.168.174.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
``

### 4. Utilizar a aplicação

Feitas as configurações tendo em conta o VMware como novo provider, podemos executar `vagrant up` para testar a aplicação.

**4.1.** Podemos utilizar as seguintes opções para verificar a VM web:

- `http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/`


- `http://192.168.174.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/`

E verificamos o resultado:

![ca3_web1](images/web_ca3_alt_1.png)

**4.2.** Testamos também a consola da base de dados H2:

- `http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console`


- `http://192.168.174.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console`

Colocamos o url para ligar à base de dados: `jdbc:h2:tcp://192.168.174.11:9092/./jpadb`

Inserimos dados na base de dados:

![ca3_web1](images/db_ca3_alt_1.png)

**4.3.** Para finalizar, podemos confirmar no frontend a introdução dos dados:

![ca3_web1](images/web_ca3_alt_2.png)

### Conclusão

Comparando as duas soluções de virtualização utilizadas neste trabalho, o VirtualBox e VMware, ainda que cada uma tenha as suas particularidades, e maneiras distintas de configuração, ambas são ferramentas relativamente simples de utilizar quando se pretende criar máquinas e redes virtuais. Se o VMware é habitualmente mais utilizado em contexto profissional, visto existirem várias soluções ao nosso dispor mediante as necessidades, o VirtualBox é uma solução open-source gratuita muitas vezes usado para fins pessoais ou aprendizagem.

Em termos de utilização não senti grandes diferenças nas duas soluções. Foi apenas necessário fazer as devidas alterações no vagrantfile, considerando o provider pretendido. O uso do Vagrant também facilitou esta utilização. Graças à automatização não foi necessário criar VM’s de forma manual no VMware.
A única diferença sentida foi na definição dos IP das máquinas virtuais. Se no VirtualBox criámos manualmente uma rede virtual host-only, no VMware aproveitámos a VMnet gerada automaticamente. Na versão Workstation Player não foi possível aceder a um host network manager.

Em termos de performance e consumo de recursos também não senti diferenças. A nível de suporte, tanto uma como outra fornecem apoio para eventuais problemas.
