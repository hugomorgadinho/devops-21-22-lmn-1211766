# Class Assignment 3

<br>

**Aluno:** Hugo Pereira

**Número de aluno:** 1211766

<br>

## Part I - Virtualization

<br>

### O que é a Virtualização?

A virtualização é uma tecnologia que permite simular, através de software, hardware real. Desta maneira, é possível simular um computador, ou seja, memória RAM, armazenamento em disco rígido ou até interfaces de rede. Estes computadores simulados através de software são denominados por **virtual machines**.

Através de um computador real (hardware físico) é assim possível "criar" vários computadores virtuais e assim "reaproveitar" o hardware de uma máquina em várias máquinas (virtuais). A máquina com hardware real é denominada por *host*, já máquina virtual é designada por *guest*. Esta é uma técnica muito interessante e que permite, por exemplo, utilizar vários sistemas operativos diferentes numa única máquina real.

Visto que a virtual machine funciona sobre uma máquina real, a capacidade do hardware simulado não deve exceder a capacidade do hardware real.

A criação de virtual machines é possível através de um software conhecido por **hipervisor**. O hipervisor pode correr (ou não) a partir de um sistema operativo host. Existem vários tipos de hipervisor disponíveis. Destacamos alguns:

- VMWare


- **Virtualbox**


- Hyper V, Microsoft Virtual Server and Virtual PC


- KVM


### VirtualBox: uma forma de criar máquinas virtuais

O **VirtualBox** é um software de virtualização da Oracle, de distribuição gratuita , que permite criar máquinas virtuais de maneira personalizada. Permite a utilização de diferentes sistemas operativos, através de diversas máquinas virtuais, numa única máquina host. É multiplataforma e está disponível para os seguintes sistemas operativos:


-  Mac OS X


- Windows


- Linux


- Oracle Solaris



No relatório técnico que a seguir se apresenta, são descritas todas as etapas de instalação e utilização do VirtualBox.



## Relatório técnico - Virtualização e uso do VirtualBox

Vamos agora passar à parte prática do relatório e utilizar o VirtualBox para conhecer melhor as potencialidades da virtualização.

### 1. Instalar VirtualBox
**1.1.** Seguindo as indicações da aula teórica, começamos por instalar o **VirtualBox** através do respectivo site. Existem vários packages de instalação para utilizadores de diferentes sistemas operativos.

<br>

### 2. Criar uma máquina virtual e configurações iniciais

**2.1.** Já com a instalação concluída, podemos agora criar uma máquina virtual (VM). Para isso, no menu inicial, basta seguir os seguintes passos:
- Clicar no botão `New`;


- Na nova janela, preencher as configurações iniciais, nomeadamente: Nome da VM, localização de origem (no nosso computador) para a máquina virtual, capacidade de memória RAM (no nosso caso, 2048 MB) e capacidade de armazenamento de disco (reservámos 10 GB);


- De seguida, clicar no botão `Create`.

**2.2.** Nos `Settings` da VM, clicamos no separador `Storage` para conectar o ficheiro ISO `mini` relativo à instalação do Ubuntu 18.04, o sistema operativo que vamos utilizar.

<br>

### 3. Instalar o Ubuntu 18.04 na VM

Para instalar o Ubuntu basta seguir as habituais indicações de instalação. No final, é importante remover o ficheiro ISO `mini`, novamente em `Settings` da VM.

<br>

### 4. Definir na VM um Host-only Adapter

Com a VM desligada, abrimos o gestor de redes. Em `Adapter 2` associamos a opção “Host-only adapter”. Depois, no campo `Name` selecionamos `"VirtualBox Host-Only Ethernet Adapter"`.

<br>

### 5. Configurações de rede na VM



**5.1.** Com a VM  ligada, continuamos a realizar as configurações de rede:

- Update repositórios de packages : `sudo apt update` ;


- Instalar network tools: `sudo apt install net-tools`;


- Atreavés do Nano, editar o ficheiro de configurações de rede: `sudo nano /etc/netplan/01-netcfg.yaml`;


- Definir o IP do `Adapter 2` para `192.168.56.5`. O conteúdo do ficheiro fica da seguinter maneira:

```
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: yes
    enp0s8:
      addresses:
        - 192.168.56.5/24
```


- Aplicar as alterações efectuadas : `sudo netplan apply`;

<br>

### 6. Instalar SSH server

**6.1.** Instalar o **Openssh-server** para assim podermos iniciar uma ligação **ssh** segura a partir do terminal do nosso computador real (*host*) para aceder à VM: `sudo apt install openssh-server`;


- Autenticar a password para o ssh: `sudo nano /etc/ssh/sshd_config`;


- Com o nano aberto, descomentar a linha `PasswordAuthentication yes`;


- A seguir fazer o restart do serviço: `sudo service ssh restart `;

<br>

### 7. Instalar FTP server

**7.1.** Instalar um `ftp server` para podermos utilizar o protocolo **FTP** de maneira a ser possível transferir ficheiros entre a VM e um computador real (*host*): `sudo apt install vsftpd`

- Permitir o acesso `write` ao vsftpd (o servidor FTP que instalámos): `sudo nano /etc/vsftpd.conf`;


- Descomentar a linha `write_enable=YES`;


- Fazer o restart do serviço: `sudo service vsftpd restart`;

<br>

### 8. Usar o SSH server

**8.1.** Com o SSH server já instalado, podemos agora fazer uso desta ligação entre o nosso computador e a VM.

-  Escrever no terminal do nosso computador (*host*): `ssh hugo@192.168.56.5`;

<br>

### 9. Usar o FTP server

**9.1.** Com o FTP server acessível na VM, podemos agora beneficiar deste protocolo através de uma aplicação. Utilizaremos o **FileZilla** para transferir conteúdos entre VM e computador host. Para isso, basta instalar a aplicação em `https://filezilla-project.org`

<br>

### 10. Instalar Git e Java

**10.1** Para instalar o Git: `sudo apt install git`

**10.2** Para instalar o Java (JDK 11): `sudo apt install openjdk-11-jdk-headless`


<br>

### 11. Utilizar a VM 

Podemos agora utilizar a nossa VM. Vamos testar o trabalho desenvolvido nos CA's anteriores.

- Para isso, começamos por fazer clone do nosso repositório para a VM: `git clone https://hugomorgadinho@bitbucket.org/hugomorgadinho/devops-21-22-lmn-1211766.git`

<br>

### 12. Run CA_1

**12.1.** Primeiro precisamos de garantir as permissões de execução do Maven Wrapper: Utilizamos o comando: `chmod u+x mvnw `.


**12.2.** Na pasta `CA1/tut-react-and-spring-data-rest/basic/` executamos o comando `./mvnw spring-boot:run`. Como esperado, no browser do computador host a aplicação fica vísivel no endereço `http://192.168.56.5:8080/`.


<br>

### 13. Run CA_2

**13.1** Para testar a **Parte 1**, a ChatApp, executaremos o servidor do lado da VM e o cliente do lado do computador host (visto ter GUI). Tal como fizemos anteriormente, é necessário antes de mais garantir as permissões de execução do Gradle Wrapper: `chmod u+x gradlew`;

<br>

**13.2.** Na VM, na pasta `CA2/Part_1/gradle_basic_demo/` executamos o server:

- Primeiro o build: `/gradlew build`;

- Depois sim, o run ao servidor: `./gradlew runServer`;

Para que do lado do cliente seja possível visualizar a aplicação no browser é necessário alterar a task `runCLient` no ficheiro `build.gradle`:

- Em `args`, onde antes estava `localhost`, deverá agora estar o IP do servidor, no nosso caso o IP da VM: `'192.168.56.5'`

<br>



**13.3.** Para testar a **Parte 2**, voltamos a dar as permissões de execução ao Gradle Wrapper: `./gradlew runServer`. Na pasta `CA2/Part_2/react-and-spring-data-rest-basic/`:

- Começamos por fazer o build da app: `./gradlew build`;


- De seguida, fazemos run da aplicação: `./gradlew bootRun `;


Abrimos o browser, na máquina host, e colocamos o endereço definido `http://192.168.56.5:8080/`. Como desejado, podemos visualizar o frontend da aplicação.